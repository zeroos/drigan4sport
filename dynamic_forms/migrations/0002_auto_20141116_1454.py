# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_hstore.fields


class Migration(migrations.Migration):

    dependencies = [
        ('dynamic_forms', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dynamicformdata',
            name='data',
            field=django_hstore.fields.DictionaryField(),
        ),
    ]
