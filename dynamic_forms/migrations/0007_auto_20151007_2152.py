# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dynamic_forms', '0006_auto_20150905_1131'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dynamicformfile',
            name='name',
            field=models.TextField(),
            preserve_default=True,
        ),
    ]
