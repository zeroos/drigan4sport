# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import dynamic_forms.validators


class Migration(migrations.Migration):

    dependencies = [
        ('dynamic_forms', '0004_auto_20141226_1213'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dynamicform',
            name='can_be_filled',
            field=models.BooleanField(default=False, verbose_name='form can be filled'),
            # preserve_default=True,
        ),
        migrations.AlterField(
            model_name='dynamicform',
            name='list_visibility',
            field=models.CharField(choices=[('overriden', '')], verbose_name='list visibility', max_length=30),
            # preserve_default=True,
        ),
        migrations.AlterField(
            model_name='dynamicformfield',
            name='field_type',
            field=models.CharField(verbose_name='field type', max_length=100, help_text='Type of data this field stores'),
            # preserve_default=True,
        ),
        migrations.AlterField(
            model_name='dynamicformfield',
            name='name',
            field=models.TextField(verbose_name='name', help_text='Name of the field, it will be displayed as label for this question', validators=[dynamic_forms.validators.ContainsNoLeadingOrTrailingWhitespace()]),
            # preserve_default=True,
        ),
    ]
