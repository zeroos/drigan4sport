# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import dynamic_forms.validators


class Migration(migrations.Migration):

    dependencies = [
        ('dynamic_forms', '0007_auto_20151007_2152'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dynamicform',
            name='can_be_filled',
            field=models.BooleanField(verbose_name='form can be filled', default=False),
        ),
        migrations.AlterField(
            model_name='dynamicform',
            name='list_visibility',
            field=models.CharField(choices=[('overriden', '')], max_length=30, verbose_name='list visibility'),
        ),
        migrations.AlterField(
            model_name='dynamicformfield',
            name='field_type',
            field=models.CharField(verbose_name='field type', help_text='Type of data this field stores', max_length=100),
        ),
        migrations.AlterField(
            model_name='dynamicformfield',
            name='name',
            field=models.TextField(validators=[dynamic_forms.validators.ContainsNoLeadingOrTrailingWhitespace()], verbose_name='name', help_text='Name of the field, it will be displayed as label for this question'),
        ),
    ]
