# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dynamic_forms', '0005_auto_20150521_1225'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dynamicformfile',
            name='file_name_for_user',
            field=models.CharField(max_length=2000),
        ),
    ]
