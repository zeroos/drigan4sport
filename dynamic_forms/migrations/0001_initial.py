# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import positions.fields
import django_hstore.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contenttypes', '0001_initial'),
    ]

    operations = [
        migrations.RunSQL(
            "CREATE EXTENSION IF NOT EXISTS hstore;",
            "DROP EXTENSION hstore;"
        ),
        migrations.CreateModel(
            name='DynamicForm',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('object_id', models.PositiveIntegerField(null=True)),
                ('can_be_filled', models.BooleanField(default=False)),
                ('list_visibility', models.CharField(choices=[('overriden', '')], max_length=30)),
                ('content_type', models.ForeignKey(null=True, to='contenttypes.ContentType')),
            ],
            options={
                'permissions': (('can_view_participants_lists', 'Can view participants lists'), ('can_edit_dynamic_form', 'Can edit dynamic forms')),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DynamicFormData',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('data', django_hstore.fields.DictionaryField(db_index=True)),
                ('form', models.ForeignKey(to='dynamic_forms.DynamicForm')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DynamicFormField',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('name', models.CharField(help_text='Name of the field, it will be displayed as label for this question', verbose_name='name', max_length=100)),
                ('field_type', models.CharField(help_text='Type of data this field stores', choices=[('DynamicTextField', 'Text Field'), ('DynamicIntegerField', 'Number Field'), ('DynamicCharField', 'String Field'), ('DynamicEmailField', 'E-mail Field'), ('DynamicDateField', 'Date Field'), ('DynamicBooleanField', 'Yes/No Field'), ('DynamicFileField', 'File field'), ('DynamicChoicesField', 'ComboBox Field'), ('DynamicPLPESELField', 'PESEL number'), ('DynamicPLNIPField', 'NIP number'), ('DynamicPLPostalCodeField', 'Postal number'), ('DynamicPLNationalIDCardNumberField', 'Polish ID card number'), ('DynamicNipOrPeselField', 'PESEL or NIP number'), ('DynamicPolishProvince', 'Polish Province'), ('DynamicPolishCounty', 'Polish County'), ('DynamicStopienInstruktorskiField', 'Stopień Instruktorski'), ('DynamicStopienHarcerskiField', 'Stopień\xa0Harcerski')], max_length=100, verbose_name='field type')),
                ('required', models.BooleanField(verbose_name='required', default=True)),
                ('additional_data', django_hstore.fields.DictionaryField(blank=True)),
                ('position', positions.fields.PositionField(default=-1)),
                ('form', models.ForeignKey(related_name='fields', to='dynamic_forms.DynamicForm')),
            ],
            options={
                'ordering': ['position'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DynamicFormFile',
            fields=[
                ('name', models.CharField(max_length=100)),
                ('file_name_in_storage', models.CharField(db_index=True, serialize=False, max_length=100, primary_key=True, unique=True)),
                ('file_name_for_user', models.CharField(max_length=100)),
                ('content_type', models.CharField(max_length=100)),
                ('data', models.ForeignKey(to='dynamic_forms.DynamicFormData')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
