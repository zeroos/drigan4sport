from .models import DynamicForm


def visible_for_user_with_view_permissions(user, form):
    if user.has_perm('dynamic_forms.can_view_participants_list'):
        return True
    return False


def visible_for_all(user, form):
    if form.list_visibility == DynamicForm.LIST_VISIBLE_FOR_ALL:
        return True
    return False


def visible_for_participants(user, form):
    if user.is_authenticated():
        if form.list_visibility == DynamicForm.LIST_VISIBLE_FOR_PARTICIPANTS \
                and (form.is_filled_by(user)):
            return True
    return False


default_access_participants_list_restrictions = [
    visible_for_user_with_view_permissions,
    visible_for_all,
    visible_for_participants,
]


def get_access_participants_list_restrictions(dynamic_form):
    """
    Returns access restrictions for given form -- they can eighter be taken
    from model's DynamicFormMeta property, or, if they are not available,
    this function will return a default one.
    """

    #TODO: This doesnt really look clear.
    if(hasattr(dynamic_form.content_object, "DynamicFormMeta") and
       hasattr(dynamic_form.content_object.DynamicFormMeta,
               "access_participants_list_restrictions")):
        return dynamic_form.content_object.DynamicFormMeta \
            .access_participants_list_restrictions
    return default_access_participants_list_restrictions
