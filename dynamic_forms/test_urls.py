# -*- coding: utf-8 -*-
import registration.auth_urls

from .urls import urlpatterns as normal_urls
from django.conf.urls import url
from . import views



urlpatterns = normal_urls + [
    url(r'^list/(?P<dynamic_form_id>\d+)/$',
        views.ParticipantList.as_view(),
        name="dynamic_forms.views.participants_list"),
    url(r'^fill/(?P<dynamic_form_id>\d+)/$',
        views.FillForm.as_view(),
        name='dynamic_forms.views.fill_form'),
    url(r'^save/(?P<dynamic_form_id>\d+)/$',
        views.SaveList.as_view(),
        name="dynamic_forms.views.save_list"),
] + registration.auth_urls.urlpatterns