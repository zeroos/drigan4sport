# -*- coding: utf-8 -*-

"""
Dynamic forms module.

Models diagram:

.. figure: dynamic_forms/dynamic_forms.png

    Dynamic forms UML diagram
"""

import sys
from dynamic_forms.validators import ContainsNoLeadingOrTrailingWhitespace

from .fieldtype import get_field, get_field_type_choices
from django.contrib.auth.models import User
from django.contrib.contenttypes import fields
from django.contrib.contenttypes.models import ContentType
from django.db import connection, models
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from django_hstore import hstore

from positions import PositionField
import re
from reversion import revisions as reversion


class FieldNameNotUniqueError(ValueError):
    pass


@reversion.register()
class DynamicForm(models.Model):

    """
    Represents a dynamic form that is a form for which fields
    can be added dynamically.
    """
    LIST_VISIBLE_FOR_ALL = "all"
    LIST_VISIBLE_FOR_PARTICIPANTS = "participants"

    content_type = models.ForeignKey(ContentType, null=True)
    object_id = models.PositiveIntegerField(null=True)
    content_object = fields.GenericForeignKey(
        'content_type', 'object_id')
    """
    Reference to object that uses this instance of Dynamic Form
    """
    can_be_filled = models.BooleanField(_("form can be filled"), default=False)
    list_visibility = models.CharField(_("list visibility"), max_length=30,
                                       choices=[('overriden', '')])

    def __init__(self, *args, **kwargs):
        super(DynamicForm, self).__init__(*args, **kwargs)
        lv = self._meta.get_field_by_name('list_visibility')[0]
        lv._choices = self.list_visibility_choices()
        lv.default = self.default_list_visibility()

    def default_list_visibility(self):
        if(hasattr(self.content_object, "DynamicFormMeta") and
           hasattr(self.content_object.DynamicFormMeta,
                   "default_list_visibility")):
            return self.content_object.DynamicFormMeta.default_list_visibility
        return self.LIST_VISIBLE_FOR_PARTICIPANTS

    def list_visibility_choices(self):
        if(hasattr(self.content_object, "DynamicFormMeta") and
           hasattr(self.content_object.DynamicFormMeta,
                   "list_visibility_choices")):
            return self.content_object.DynamicFormMeta.list_visibility_choices
        return (
            (self.LIST_VISIBLE_FOR_ALL, _("everyone")),
            (self.LIST_VISIBLE_FOR_PARTICIPANTS, _("all participants")),
        )

    def add_field_to_form(self, field):

        if self.fields.filter(name__iexact=field.name).exists():
            raise FieldNameNotUniqueError(
                "Field with the same name is already added to a form")

        field.form = self

    def is_filled_by(self, user):
        """
        Returns `True` if this form was filled by `user`
        """
        return self.dynamicformdata_set.filter(user=user).exists()

    def can_edit_dynamic_form(self, user):
        if user.has_perm('dynamic_forms.can_edit_dynamic_form'):
            return True

        try:
            return self.content_object.DynamicFormMeta \
                       .can_edit(user, self)
        except AttributeError:
            return False

    def can_see_participants_list(self, user):
        if user.has_perm('dynamic_forms.can_view_participants_list'):
            return True

        from .list_visibility_restrictions import \
            get_access_participants_list_restrictions
        # deferred import -- it would cause circular dependency if imported in
        # the module

        return any([r(user, self) for r in
                    get_access_participants_list_restrictions(self)
                    ])

    def can_save_participants_list(self, user):
        if user.has_perm('dynamic_forms.can_view_participants_list'):
            return True
        try:
            return self.content_object.DynamicFormMeta \
                       .can_edit(user, self)
        except AttributeError:
            return False

    def can_see_uploaded_files(self, user):
            return self.can_see_participants_list(user)

    class Meta:
        permissions = (("can_view_participants_lists",
                        _("Can view participants lists")),
                       ("can_edit_dynamic_form",
                        _("Can edit dynamic forms")),)


@reversion.register()
class DynamicFormField(models.Model):

    """
    Single field in a dynamic form
    """
    name = models.TextField(
        _("name"),
        help_text=_("Name of the field, it will be displayed as "
                    "label for this question"),
        validators=[ContainsNoLeadingOrTrailingWhitespace()])

    field_type = models.CharField(
        _("field type"),
        max_length=100,
        help_text=_("Type of data this field stores"))

    required = models.BooleanField(_("required"), default=True)
    form = models.ForeignKey(DynamicForm, related_name='fields')
    """
    Form for which this object
    """
    additional_data = hstore.DictionaryField(
        blank=True, null=False)
    """
    Dictionary of additional data, contents are defined by:
    :attr:`DynamicFormField.field_type`
    """
    position = PositionField(collection='form')

    objects = hstore.HStoreManager()

    def get_django_field(self):
        return self.dynamic_field.load_field(self)

    @property
    def dynamic_field(self):
        """
        Returns instance of :class:`DynamicFieldController`, object
        that encapsulates behaviour of this field.

        It is depenedent on `DynamicFormField.field_type`
        :return: :class:`DynamicFieldController`,
        :rtype: :class:`DynamicFieldController`,
        """
        return get_field(self.field_type)

    def wrap_data(self, data):
        """
        Returns data wrapped in an object representing given file type.
        """
        return self.dynamic_field.wrap_data(data, self)

    class Meta:
        ordering = ['position']


@reversion.register()
class DynamicFormData(models.Model):
    """
    Response to dynamic form
    """
    form = models.ForeignKey(DynamicForm)
    user = models.ForeignKey(User)
    data = hstore.DictionaryField()

    fill_date = models.DateTimeField(auto_now_add=True)

    objects = hstore.HStoreManager()

    confirmed = models.BooleanField(default=False)

    def __unicode__(self):
        return u'%s %s' % (self.form, self.user)

    def __str__(self):
        return str(self.data)

    @property
    def data_ordered(self):
        """
        Returns list which includes data ordered the same way as fields in form.
        :rtype: list of tuples
        """
        data_ordered = []
        for dynamic_field in self.form.fields.all():
            name = dynamic_field.name
            if name not in self.data:
                data_ordered.append((name, None))
            else:
                data_ordered.append((name,
                                     dynamic_field.wrap_data(self.data[name])))
        return data_ordered

    class Meta:
        # TODO: This should be ordered only using fill_date
        # but since we have some old instances in the DB they need to be sorted
        # using PK
        ordering = ['fill_date', 'pk']


@reversion.register()
class DynamicFormFile(models.Model):

    data = models.ForeignKey(DynamicFormData)
    name = models.TextField()
    file_name_in_storage = models.CharField(max_length=100, unique=True,
                                            db_index=True, primary_key=True)
    file_name_for_user = models.CharField(max_length=2000)
    content_type = models.CharField(max_length=100)

    @classmethod
    def clean_filename_for_user(cls, filename):
        """
        >>> DynamicFormFile.clean_filename_for_user("Żażółć gęślą jaźń")
        :param filename:
        :return:
        """
        return re.sub("[^a-zA-Z \d]", "_", filename)



from . import default_fieldtypes  # For side effect  # NOQA
