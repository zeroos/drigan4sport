# -*- coding: utf-8 -*-
from django.core.files.storage import FileSystemStorage
from django.conf import settings


class DynamicFormsPrivateStorage(FileSystemStorage):
    def __init__(self, base_url=None):
        super().__init__(settings.DYNFORM_PRIVATE_MEDIA_ROOT, base_url)