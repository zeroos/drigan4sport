from django.contrib import admin
from dynamic_forms.models import DynamicFormField, DynamicForm, DynamicFormData, \
    DynamicFormFile

from reversion.admin import VersionAdmin


class DynamicFormAdmin(VersionAdmin):

    pass


admin.site.register(DynamicForm, DynamicFormAdmin)
admin.site.register(DynamicFormField, DynamicFormAdmin)
admin.site.register(DynamicFormData, DynamicFormAdmin)
admin.site.register(DynamicFormFile, DynamicFormAdmin)
