# -*- coding: utf-8 -*-
from django.core.validators import BaseValidator
from django.utils.translation import ugettext_lazy


class ContainsNoLeadingOrTrailingWhitespace(BaseValidator):

    compare = lambda self, a, b: a != a.strip()
    clean = lambda self, x: x
    message = ugettext_lazy("This value can't contain leading or trailing whitespace")
    code = 'no_leading_whitespace'

    def __init__(self):
        super().__init__(None)