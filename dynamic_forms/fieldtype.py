# -*- coding: utf-8 -*-

import abc
from django.forms.fields import ChoiceField, Field
from django.utils.html import escape


_FIELD_TYPES_DICT = {}

_FIELD_TYPE_CHOICES = []

__all__ = [
    'register_field_type', 'get_field', 'get_field_type_choices',
    'DynamicFieldController',
    'ChoiceField'
]


#TODO: We really should store types and instantiate them during
# DynamicFormField.field_type property call, in this case, for example
# a request to DynamicFormField could be stored inside instance of new
# class. Now we store instances --- which could wreak havoc if any of them
# store request state.
def register_field_type(name):
    """
    Registers field type for name if you decorate a type with it

    >>> @register_field_type("OtherChoicesField")
    ... class OtherChoicesField(ChoicesField): pass

    >>> isinstance(get_field("OtherChoicesField"), OtherChoicesField)
    True

    :param name: Name under which to store decorated type
    """
    def wrapper(clazz):
        clazz.FIELD_NAME = name
        field = clazz()
        _FIELD_TYPES_DICT[name] = field
        _FIELD_TYPE_CHOICES.append((name, field.get_type_description()))
        return clazz
    return wrapper


def get_field(name):
    """
    :param str name:
    :return: Returns field type by name
    :rtype: :class:`DynamicFieldController`
    """
    return _FIELD_TYPES_DICT[name]


def get_field_type_choices():
    """
    Returns django choices dictionary containing dynamic fields.
    """
    return _FIELD_TYPE_CHOICES


class BaseWrapper(object):
    """
    Base Wrapper -- a classes inheriting from this one would be used to
    represent data collected with a dynamic field. For each dynamic
    field type a different wrapper class should be defined.
    """
    def __init__(self, data, dynamic_field=None):
        self.data = data
        self.dynamic_field = dynamic_field

    def as_text(self):
        return self.data

    def as_html(self):
        return escape(self.as_text())

    def __str__(self):
        # if self.as_text() returns a something that is not a string eg. None
        # this will result in exception. So to be safe we convert to `str`.
        return str(self.as_text())
        # return self.as_text()


class DynamicFieldController(object, metaclass=abc.ABCMeta):  # NOQA

    """
    Main class of this API. It is responsible for creating dynamic fields
    from the database.

    """

    FIELD_NAME = None
    """
    A class variable. subclasses **must** override this
    as this field defines type of the field that particular subclass
    will create. This is a string.
    """

    _NO_SUCH_OBJECT = object()

    WRAPPER = BaseWrapper
    """
    Defines a class that will be used to wrap data related to this type
    of field
    """

    @abc.abstractmethod
    def get_type_description(self):
        """

        :return: Returns human redable description of this controller.
        :rtype: :class:`str`
        """
        return None

    def load_field(self, dynamic_field):
        """
        Creates django field from this dynamic field. This metod takes
        care of common operations done for all fields. Subclasses need
        to override :meth:`DynamicFieldController._create_field`.

        :param dynamic_field:
        :return: Django field
        :rtype: Subclass of :class:`django.forms.fields.Field`
        """
        field = self._create_field()
        field.required = dynamic_field.required
        field.label = dynamic_field.name
        return field

    @abc.abstractmethod
    def _create_field(self):
        """
        Creates django field from this dynamic field. This should not be called
        directly in favour of: :meth:`DynamicFieldController.load_field`.

        This should be overriden.

        :return: Django field
        :rtype: Subclass of :class:`django.forms.fields.Field`
        """
        return None

    def serialize_field_content(self, name, item, form, request, model):
        """
        Function is used to serialize data -- usually it can just return
        input data, but sometimes some processing might be needed, for example
        to return a dictionary containing an information about a file
        or to standarize date format.
        """
        return item

    def wrap_data(self, data, dynamic_field=None):
        """
        Wraps given data into a Wrapper object in order to be able for example
        to display a day of a date, or filename of a file.

        :param str data:
        :param dynamic_field:
        :type dynamic_field: :class:`dynamic_forms.models.DynamicFormField`
        """
        return self.WRAPPER(data, dynamic_field)


class _DjangoDynamicFieldController(DynamicFieldController):
    """
    DynamicFormController that simply wraps a DjangoField.

    :cvar type DJANGO_FIELD_TYPE: Type of the django field stored by this
        Controller.
    :cvar str DESCRIPTION: Human readable description of the field.
    """

    DESCRIPTION = None

    DJANGO_FIELD_TYPE = Field

    def get_type_description(self):
        return self.DESCRIPTION

    def _create_field(self):
        return self.DJANGO_FIELD_TYPE()


def create_dynamic_field_from_django_form(django_type, description, name=None):

    """
    Creates a dynamic field from django field.
    :param type django_type: Type that will be wrapped.
    :param str description: Human readable desctiption
    :return: None
    """

    if not issubclass(django_type, Field):
        raise ValueError("You are creating a dynamic field from a something that "
                         "does not derive from django field.")

    if name is None:
        name = "Dynamic" + django_type.__name__
    clazz_name = name
    clazz = type(clazz_name,
                 (_DjangoDynamicFieldController, ),
                 {
                    "DESCRIPTION": description,
                    "DJANGO_FIELD_TYPE": django_type,
                 })

    register_field_type(clazz.__name__)(clazz)


def create_dynamic_field_from_a_callable(callable, FieldName, description):
    """
    Creates dynamic field type (identical to one created by
    :meth:`create_dynamic_field_from_django_form` by specifying a callable
    field name and a description.

    :param callable callable:
    :param str FieldName:
    :param str description:
    """

    clazz = type("Dynamic" + FieldName, (_DjangoDynamicFieldController, ), {
        "DESCRIPTION": description,
        "DJANGO_FIELD_TYPE": callable
    })

    register_field_type(clazz.__name__)(clazz)
