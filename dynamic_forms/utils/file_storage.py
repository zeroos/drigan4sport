# -*- coding: utf-8 -*-
from django.conf import settings
from django.core.files.storage import get_storage_class
from django.utils.functional import LazyObject
from django.utils.module_loading import import_string


class DriganFileStorage(LazyObject):
    def _setup(self):
        if not hasattr(settings, 'DYNFORM_PRIVATE_FILE_STORAGE'):
            self._wrapped = get_storage_class()()
        else:
            self._wrapped = import_string(settings.DYNFORM_PRIVATE_FILE_STORAGE)()