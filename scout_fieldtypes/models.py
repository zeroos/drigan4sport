from .polish_fieldtypes import create_fieldtypes as polish_fieldtypes
from .scout_fieldtypes import StopienHarcerskiField, StopienInstruktorskiField
from dynamic_forms.fieldtype import register_field_type, _DjangoDynamicFieldController
from django.utils.translation import ugettext_lazy


polish_fieldtypes()


class DynamicDefinedChoicesField(_DjangoDynamicFieldController):

    def load_field(self, dynamic_field):
        choice_field = super().load_field(dynamic_field)
        choices = choice_field.choices
        if not dynamic_field.required:
            choices.insert(0, ('', "-" * 7))
        choice_field.choices = choices
        return choice_field


@register_field_type("DynamicStopienHarcerskiField")
class DynamicStopienHarceskiField(DynamicDefinedChoicesField):

    DESCRIPTION = ugettext_lazy("Stopień Harcerski")
    DJANGO_FIELD_TYPE = StopienHarcerskiField


@register_field_type("DynamicStopienInstruktorskiField")
class DynamicStopienInstruktorskiField(DynamicDefinedChoicesField):

    DESCRIPTION = ugettext_lazy("Stopień Instruktorski")
    DJANGO_FIELD_TYPE = StopienInstruktorskiField
