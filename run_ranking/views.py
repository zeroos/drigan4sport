from django.views.generic.list import ListView
from django.views.generic.edit import FormView
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator
import datetime
import csv
import codecs
import string
from django.http import HttpResponseRedirect
from django.utils.text import slugify
from django.contrib import messages


from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from django.utils.translation import pgettext
from django.db import transaction
from random import SystemRandom

from dynamic_forms.models import DynamicFormData
from dynamic_forms.views import FillForm

from run_ranking.models import Participant
from run_manager.models import Competition, Run, RunEvent

from run_ranking.ranking import Ranking, Category, RankingJoin
from run_ranking.forms import ImportForm, ParticipantForm


@method_decorator(permission_required('run_manager.add_run'), name='dispatch')
class ImportParticipants(FormView):
    template_name = 'run_ranking/import_participants.html'
    form_class = ImportForm
    success_url = ''

    def form_valid(self, form):
        competition = form.cleaned_data['competition']
        categories = Category.objects.filter(competition=competition)
        cat_dict = {c.name: c for c in categories}
        f = competition.attraction.forms[0]
        csvfile = form.cleaned_data['data_file']
        csv_reader = csv.DictReader(codecs.iterdecode(csvfile, 'utf-8'))
        with transaction.atomic():
            for line in csv_reader:
                d = DynamicFormData()
                d.form = f
                rand = SystemRandom()
                rand_name = "".join([rand.choice(string.ascii_letters + string.digits) for __ in range(15)])
                username_pattern = "anonymous-{}".format(rand_name)
                user = User(
                    username=username_pattern
                )
                user.set_unusable_password()
                user.save()
                d.user = user
                d.data = {k: line[k] for k in line.keys() if k != 'kategoria' and k != 'numer'}
                d.save()
                p = Participant()
                p.identity = line['numer']
                p.participant = d
                p.category = cat_dict[line['kategoria']]
                p.competition = competition
                p.save()
        return HttpResponseRedirect(self.success_url)


@method_decorator(permission_required('run_manager.add_run'), name='dispatch')
class ImportTimes(FormView):
    template_name = 'run_ranking/import.html'
    form_class = ImportForm
    success_url = ''

    def form_valid(self, form):
        competition = form.cleaned_data['competition']
        csvfile = form.cleaned_data['data_file']
        csv_reader = csv.reader(codecs.iterdecode(csvfile, 'utf-8'),
                                delimiter=' ')
        for line in csv_reader:
            r = Run()
            r.competition = competition
            r.save()
            e1 = RunEvent()
            e1.name = 'confirmed_id'
            e1.data = line[0]
            e1.datetime = datetime.datetime.now()
            e1.run = r
            e1.competition = competition
            e1.save()
            e2 = RunEvent()
            e2.name = 'time'
            try:
                if line[1] == 'dnf' or line[1] == 'dsq' or line[1] == 'dns':
                    e2.data = '0:0.0'
                else:
                    e2.data = '0:' + line[1]
            except IndexError:
                e2.data = '0:0.0'
            e2.datetime = datetime.datetime.now()
            e2.run = r
            e2.competition = competition
            e2.save()
        return HttpResponseRedirect(self.success_url)


@method_decorator(permission_required('run_manager.add_run'), name='dispatch')
class StartListView(ListView):
    template_name = 'run_ranking/start_list.html'

    def get_participants(self, participants, competition):
        participant_list = []

        for p in participants:
            d = {}
            d['name'] = p.name
            d['surname'] = p.surname
            d['sex'] = p.data('Płeć')
            d['identity'] = p.identity
            d['category'] = p.category.name
            participant_list.append(d)

        return participant_list

    def get_header(self, participants):
        header = ['#', pgettext('participant name', 'Name'), _('Surname'),
                  _('Number'), _('Category')]
        return header

    def get_queryset(self):
        competition = get_object_or_404(Competition,
                                        id=self.kwargs['competition'])
        return Participant.objects.filter(competition=competition) \
            .prefetch_related("participant") \
            .prefetch_related("participant__form") \
            .prefetch_related("participant__form__fields") \
            .prefetch_related("category")

    def get_context_data(self, **kwargs):
        competition = get_object_or_404(Competition,
                                        id=self.kwargs['competition'])
        context = super().get_context_data(**kwargs)
        context['competition'] = competition
        participants = self.get_participants(context['object_list'],
                                             competition)
        ranking = Ranking(participants, competition, 'list')
        context['ranking'] = ranking.start_list
        context['header'] = self.get_header(participants)
        return context


@method_decorator(permission_required('run_manager.add_run'), name='dispatch')
class RankingView(StartListView):
    template_name = 'run_ranking/ranking.html'

    def get_runs(self, competition):
        runs = Run.objects.filter(competition=competition) \
            .prefetch_related("runevent_set")
        run_dict = {}
        for r in runs:
            identity = r.identity
            if identity is None:
                continue
            identity = identity.data
            if identity in run_dict:
                run_dict[identity].append(r)
            else:
                run_dict[identity] = [r, ]
        return run_dict

    def get_participants(self, participants, competition):
        runs = self.get_runs(competition)
        participant_list = super().get_participants(participants, competition)
        max_runs = max([len(runs[i]) for i in runs], default=0)

        for p in participant_list:
            try:
                p['runs'] = runs[p['identity']]
            except KeyError:
                p['runs'] = []
            while len(p['runs']) < max_runs:
                p['runs'].append('')

        return participant_list

    def get_header(self, participants):
        max_runs = max([len(p['runs']) for p in participants], default=0)
        header = [_('Rank'), pgettext('participant name', 'Name'),
                  _('Surname'), _('Number')]
        for i in range(max_runs):
            header.append(_('Run {}').format(str(i+1)))
        header.append(_('Result'))
        return header

    def get_context_data(self, **kwargs):
        competition = get_object_or_404(Competition,
                                        id=self.kwargs['competition'])
        context = super().get_context_data(**kwargs)
        context['competition'] = competition
        participants = self.get_participants(context['object_list'],
                                             competition)
        ranking = Ranking(participants, competition,
                          dict(Competition.TYPE_CHOICES)[competition.competition_type])
        context['ranking'] = ranking.ranking
        context['header'] = self.get_header(participants)
        return context


@method_decorator(permission_required('run_manager.add_run'), name='dispatch')
class RankingOpenView(RankingView):
    def get_context_data(self, **kwargs):
        competition = get_object_or_404(Competition,
                                        id=self.kwargs['competition'])
        context = super().get_context_data(**kwargs)
        context['competition'] = competition
        participants = self.get_participants(context['object_list'],
                                             competition)
        ranking = Ranking(participants, competition,
                          dict(Competition.TYPE_CHOICES)[competition.competition_type],
                          o=True)
        context['ranking'] = ranking.ranking
        context['header'] = self.get_header(participants)
        return context


class RankingJoinView(StartListView):
    template_name = 'run_ranking/ranking.html'

    def get_runs(self, competition):
        runs = Run.objects.filter(competition=competition) \
            .prefetch_related("runevent_set")
        run_dict = {}
        for r in runs:
            identity = r.identity
            if identity is None:
                continue
            identity = identity.data
            if identity in run_dict:
                run_dict[identity].append(r)
            else:
                run_dict[identity] = [r, ]
        return run_dict

    def get_queryset(self):
        return Participant.objects.none()

    def get_participants(self, competitions):
        runs = {}
        participants = {}
        for c in competitions:
            runs[c.name] = self.get_runs(c)
            p = Participant.objects.filter(competition=c) \
                .prefetch_related("participant") \
                .prefetch_related("participant__form") \
                .prefetch_related("participant__form__fields") \
                .prefetch_related("category")




            participants[c.name] = super().get_participants(p, c)
            for p in participants[c.name]:
                try:
                    p['runs'] = runs[c.name][p['identity']]
                except KeyError:
                    p['runs'] = []
        return participants

    def get_header(self):
        header = [_('Rank'), pgettext('participant name', 'Name'),
                  _('Surname'), _('Number')]
        header.append(_('Slalom'))
        header.append(_('Gigant'))
        header.append(_('Result'))
        return header

    def get_context_data(self, **kwargs):
        competition_ids = [int(c) for c in self.kwargs['competitions']
                           .split(',')]
        competitions = []
        for i in competition_ids:
            competitions.append(get_object_or_404(Competition, id=i))
        context = {}
        competition = {}
        competition['name'] = 'Ranking łączony'
        context['competition'] = competition
        participants = self.get_participants(competitions)
        types = [dict(Competition.TYPE_CHOICES)[c.competition_type]
                 for c in competitions]
        ranking = RankingJoin(participants, competitions, types)
        context['ranking'] = ranking.ranking
        context['header'] = self.get_header()
        return context


@method_decorator(permission_required('run_manager.add_run'), name='dispatch')
class AddParticipant(FillForm):
    form_class = ParticipantForm
    template_name = 'run_ranking/import.html'
    success_url = ''

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['competition'] = self.kwargs['competition']
        return kwargs

    def form_valid(self, form):
        user = self.request.user
        if not user.is_authenticated():
            rand = SystemRandom()
            rand_name = "".join([rand.choice(string.ascii_letters + string.digits) for __ in range(15)])
            username_pattern = "anonymous-{}".format(rand_name)
            user = User(
                username=username_pattern
            )
            user.set_unusable_password()
            user.save()

        with transaction.atomic():
            dfd = DynamicFormData.objects.create(form=self.dynamic_form,
                                                 user=user,
                                                 data={})

            data = form.cleaned_data

            serialized_data = {
                f.name: f.dynamic_field.serialize_field_content(
                    f.name, data.get(slugify(f.name), None), form, self.request, dfd)
                for f in self.dynamic_form.fields.all()}
            dfd.data = serialized_data
            dfd.save()
            competition = Competition.objects.get(id=self.kwargs['competition'])
            participant = Participant.objects.create(identity=data['identity'],
                                                     participant=dfd,
                                                     category=data['category'],
                                                     competition=competition)
            participant.save()
        return HttpResponseRedirect(self.success_url)

