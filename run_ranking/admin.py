from django.contrib import admin
from run_ranking.models import Category, Participant

admin.site.register(Category)
admin.site.register(Participant)
