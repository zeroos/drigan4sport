from django.conf.urls import patterns, url
from run_ranking import views

urlpatterns = patterns(
    '',
    url(r'^(?P<competition>\d+)$', views.RankingView.as_view(),
        name='ranking'),
    url(r'^(?P<competition>\d+)/list$', views.StartListView.as_view(),
        name='start-list'),
    url(r'^(?P<competition>\d+)/open$', views.RankingOpenView.as_view(),
        name='ranking-open'),
    url(r'^join/(?P<competitions>[\d,]+)/(?P<type>\w+)$',
        views.RankingJoinView.as_view(),
        name='ranking-join'),
    url(r'^import/runs$', views.ImportTimes.as_view(),
        name='import-runs'),
    url(r'^import/participants$', views.ImportParticipants.as_view(),
        name='import-participants'),
    url(r'^add/participant/(?P<competition>\d+)/(?P<dynamic_form_id>\d+)$',
        views.AddParticipant.as_view(),
        name='add-participant'),
)
