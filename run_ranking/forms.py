from django import forms

from dynamic_forms.forms import BaseDynamicForm
from run_ranking.models import Competition, Category


class ImportForm(forms.Form):
        competition = forms.ModelChoiceField(queryset=Competition.objects.all(),
                                             required=True)
        data_file = forms.FileField(required=True)


class ParticipantForm(BaseDynamicForm):
    def __init__(self, dynamic_form, competition, *args, **kwargs):
        #competition = kwargs.pop('competition', False)
        super(ParticipantForm, self).__init__(dynamic_form, *args, **kwargs)
        self.fields['identity'] = forms.CharField()
        self.fields['category'] = forms.ModelChoiceField(
            queryset=Category.objects.filter(competition=competition))
