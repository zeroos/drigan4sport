from django.db import models

from run_manager.models import Competition
from dynamic_forms.models import DynamicFormData


class Category(models.Model):
    name = models.CharField(max_length=100)
    competition = models.ForeignKey(Competition, null=True, blank=True)
    seq = models.IntegerField()

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ("competition", "seq")
        ordering = ['seq']


class Participant(models.Model):
    identity = models.TextField()
    participant = models.ForeignKey(DynamicFormData)
    category = models.ForeignKey(Category)
    competition = models.ForeignKey(Competition)

    @property
    def name(self):
        data = dict(self.participant.data_ordered)
        try:
            return data['imię']
        except KeyError:
            return data['Imię']

    @property
    def surname(self):
        data = dict(self.participant.data_ordered)
        try:
            return data['nazwisko']
        except KeyError:
            return data['Nazwisko']

    def data(self, key):
        data = dict(self.participant.data_ordered)
        try:
            return data[key]
        except KeyError:
            return ''

    def __str__(self):
        return self.identity
    class Meta:
        unique_together = ("competition", "identity")
