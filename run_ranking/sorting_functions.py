from run_ranking.models import Category

def cmp_best():
    pass


def cmp_sum():
    pass


def cmp_id():
    pass


CMP_FUNCTIONS = {
    'best': cmp_best,
    'sum': cmp_sum,
    'list': cmp_id,
}


class Ranking:
    def __init__(self, participants, competition):
        self.participants = participants
        self.categories = Category.objects.filter(competition=competition)
        self.ranking = self.create_ranking()

    def create_ranking(self):
        split_dict = {}
        for p in self.participants:
            try:
                category = p.category.name
            except AttributeError:
                category = 'none'
            if category in split_dict:
                split_dict[category].append(p)
            else:
                split_dict[category] = [p, ]
        ranking = [(k.name, split_dict[k.name]) for k in self.categories]
        try:
            ranking.append(('none', split_dict['none']))
        except KeyError:
            pass
        return ranking
