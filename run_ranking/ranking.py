from run_ranking.models import Category
from run_manager.models import Run
from datetime import timedelta


class Result:
    def __init__(self, time=None, dnf=False, dsq=False, dns=False):
        self.time = time
        self.dnf = dnf
        self.dsq = dsq
        self.dns = dns

    def has_time(self):
        if self.time is not None:
            return True
        else:
            return False

    def is_dnf(self):
        return self.dnf

    def is_dsq(self):
        return self.dsq

    def is_dns(self):
        return self.dns

    @staticmethod
    def format_time(td):
        hours, remainder = divmod(td.seconds, 3600)
        minutes, seconds = divmod(remainder, 60)

        timestamp = "{0:01d}:{1:02d}.{2:02d}".format(
            minutes,
            seconds,
            int(td.microseconds/10000)
        )
        return timestamp

    @property
    def display_time(self):
        time = self.time
        if self.is_dsq():
            return 'DSQ'
        if self.is_dnf():
            return 'DNF'
        if self.is_dns():
            return 'DNS'
        if time is None:
            return ''
        return self.format_time(time)

    def __str__(self):
        return self.display_time


def choose_best(runs):
    if not runs:
        return None
    best = runs[0]
    for r in runs[1:]:
        try:
            if not r.is_dnf() and not r.is_dsq() and r.time is not None:
                try:
                    if best.is_dnf() or best.is_dsq() or best.time is None:
                        best = r
                    else:
                        best = min(r, best, key=lambda x: x.time)
                except AttributeError:
                    best = r
        except AttributeError:
            pass
    return best


def get_sum(runs):
    run_sum = timedelta(seconds=0)
    for r in runs:
        try:
            run_sum += r.time
        except TypeError:
            pass
    return run_sum


def key_best(p):
    b = choose_best(p['runs'])
    if b is None or b == '' or b.time is None:
        return (2, None)
    try:
        if b.is_dnf() or b.is_dsq():
            return (1, None)
        else:
            return (0, b.time)
    except AttributeError:
        return (2, None)


def key_sum(p):
    runs = p['runs']

    if not runs:
        return (3, -len([r for r in runs if r != '']), None)
    for r in runs:
        try:
            if r.is_dnf() or r.is_dsq():
                return (1, -len([r for r in runs if r != '']), None)
        except AttributeError:
            return(1, -len([r for r in runs if r != '']), None)
    return (0, -len(runs), get_sum(runs))


def key_id(p):
    return int(p['identity'])


def result_best(p):
    b = choose_best(p['runs'])
    r = Result()
    try:
        r.time = b.time
    except AttributeError:
        try:
            if b.is_dnf():
                r.dnf = True
            elif b.is_dsq():
                r.dsq = True
            elif b.is_dns():
                r.dns = True
        except AttributeError:
            pass
    return r


def result_sum(p):
    runs = p['runs']
    empty = True
    clean_runs = []
    result = Result()
    if not runs:
        return result
    for r in runs:
        if r == '':
            continue
        empty = False
        if r.is_dnf():
            result.dnf = True
            return result
        elif r.is_dsq():
            result.dsq = True
        else:
            if r.time is not None:
                clean_runs.append(r)
    if result.dsq:
        return result
    if empty or not clean_runs:
        return result
    result.time = get_sum(clean_runs)
    return result


KEYS = {
    'best': key_best,
    'sum': key_sum,
    'list': key_id,
}

RESULTS = {
    'best': result_best,
    'sum': result_sum,
}


class Ranking:
    def __init__(self, participants, competition, t, o=False):
        self.participants = participants
        self.categories = Category.objects.filter(competition=competition)
        self.competition = competition
        if t == 'list':
            self.start_list = self.create_list()
        elif o:
            self.ranking = self.create_ranking_open(t)
        else:
            self.ranking = self.create_ranking(t)

    def create_ranking(self, t):
        split_dict = {}
        for p in self.participants:
            category = p['category']
            if category in split_dict:
                split_dict[category].append(p)
            else:
                split_dict[category] = [p, ]
        ranking = [(c.name, sorted(split_dict[c.name], key=KEYS[t]))
                   for c in self.categories
                   if c.name in split_dict]
        for j in range(len(ranking)):
            c, l = ranking[j]
            for i in range(len(l)):
                '''try:
                    l[i]['result'] = Run.format_time(RESULTS[t](l[i]))
                except AttributeError:
                    l[i]['result'] = RESULTS[t](l[i])
                '''
                l[i]['result'] = RESULTS[t](l[i])
                if i == 0:
                    l[i]['rank'] = 1
                else:
                    if KEYS[t](l[i]) == KEYS[t](l[i-1]):
                        l[i]['rank'] = l[i-1]['rank']
                    else:
                        l[i]['rank'] = i + 1
            ranking[j] = (c, l)

        return ranking

    def create_ranking_open(self, t):
        ranking = [('OPEN', sorted(self.participants, key=KEYS[t]))]
        for j in range(len(ranking)):
            c, l = ranking[j]
            for i in range(len(l)):
                try:
                    l[i]['result'] = Run.format_time(RESULTS[t](l[i]))
                except AttributeError:
                    l[i]['result'] = RESULTS[t](l[i])

                if i == 0:
                    l[i]['rank'] = 1
                else:
                    if KEYS[t](l[i]) == KEYS[t](l[i-1]):
                        l[i]['rank'] = l[i-1]['rank']
                    else:
                        l[i]['rank'] = i + 1
            ranking[j] = (c, l)

        return ranking

    def create_list(self):
        start_list = sorted(self.participants, key=KEYS['list'])
        for i in range(len(start_list)):
            start_list[i]['no'] = i + 1

        return start_list


class RankingJoin:
    def __init__(self, participants, competitions, t):
        self.participants = []
        p1 = participants[competitions[0].name]
        p2 = participants[competitions[1].name]
        for p in p1:
            for pp in p2:
                if pp['identity'] == p['identity']:
                    r = {}
                    r['identity'] = p['identity']
                    r['name'] = p['name']
                    r['surname'] = p['surname']
                    r['sex'] = p['sex']
                    l = []
                    r1 = RESULTS[t[0]](p)
                    if r1.time is not None or r1.dsq or r1.dnf or r1.dns:
                        l.append(r1)
                    else:
                        l.append('')
                    r2 = RESULTS[t[1]](pp)
                    if r2.time is not None or r2.dsq or r2.dnf or r2.dns:
                        l.append(r2)
                    else:
                        l.append('')
                    r['runs'] = l
                    r['result'] = RESULTS['sum'](r)
                    self.participants.append(r)
        self.ranking = self.create_ranking('sum')

    def create_ranking(self, t):
        split_dict = {}
        self.categories = ['KOBIETY', 'MĘŻCZYŹNI']
        for p in self.participants:
            if str(p['sex']) == 'kobieta':
                category = 'KOBIETY'
            elif str(p['sex']) == 'mężczyzna':
                category = 'MĘŻCZYŹNI'
            else:
                category = ''
            if category in split_dict:
                split_dict[category].append(p)
            else:
                split_dict[category] = [p, ]
        ranking = [(c, sorted(split_dict[c], key=KEYS[t]))
                   for c in self.categories
                   if c in split_dict]
        for j in range(len(ranking)):
            c, l = ranking[j]
            for i in range(len(l)):
                '''try:
                    l[i]['result'] = Run.format_time(RESULTS[t](l[i]))
                except AttributeError:
                    l[i]['result'] = RESULTS[t](l[i])
                '''
                l[i]['result'] = RESULTS[t](l[i])
                if i == 0:
                    l[i]['rank'] = 1
                else:
                    if KEYS[t](l[i]) == KEYS[t](l[i-1]):
                        l[i]['rank'] = l[i-1]['rank']
                    else:
                        l[i]['rank'] = i + 1
            ranking[j] = (c, l)

        return ranking

    def create_ranking_open(self, t):
        ranking = [('OPEN', sorted(self.participants, key=KEYS[t]))]
        for j in range(len(ranking)):
            c, l = ranking[j]
            for i in range(len(l)):
                try:
                    l[i]['result'] = Run.format_time(RESULTS[t](l[i]))
                except AttributeError:
                    l[i]['result'] = RESULTS[t](l[i])

                if i == 0:
                    l[i]['rank'] = 1
                else:
                    if KEYS[t](l[i]) == KEYS[t](l[i-1]):
                        l[i]['rank'] = l[i-1]['rank']
                    else:
                        l[i]['rank'] = i + 1
            ranking[j] = (c, l)

        return ranking
