# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_auto_20141016_1115'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attraction',
            name='end_date',
            field=models.DateTimeField(verbose_name='end date', help_text='Please provide date and time according to the following example 1985-09-19 21:30:00.\n    You may omit the time part.'),
        ),
        migrations.AlterField(
            model_name='attraction',
            name='event',
            field=models.ForeignKey(to='events.Event', related_name='attractions'),
        ),
        migrations.AlterField(
            model_name='attraction',
            name='start_date',
            field=models.DateTimeField(verbose_name='start date', help_text='Please provide date and time according to the following example 1985-09-19 21:30:00.\n    You may omit the time part.'),
        ),
        migrations.AlterField(
            model_name='event',
            name='end_date',
            field=models.DateTimeField(null=True, verbose_name='end date of the event', help_text='Please provide date and time according to the following example 1985-09-19 21:30:00.\n    You may omit the time part.', blank=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='start_date',
            field=models.DateTimeField(null=True, verbose_name='start date of the event', help_text='Please provide date and time according to the following example 1985-09-19 21:30:00.\n    You may omit the time part.', blank=True),
        ),
    ]
