# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0003_auto_20141116_1454'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='attraction',
            options={'verbose_name_plural': 'attractions', 'verbose_name': 'attraction', 'ordering': ['start_date', 'name']},
        ),
        migrations.AlterModelOptions(
            name='event',
            options={'verbose_name_plural': 'events', 'verbose_name': 'event'},
        ),
        migrations.AlterModelOptions(
            name='organizer',
            options={'verbose_name_plural': 'organizers', 'verbose_name': 'organizer'},
        ),
        migrations.AlterField(
            model_name='attraction',
            name='end_date',
            field=models.DateTimeField(verbose_name='end date', help_text='Please provide date and time according to the following example 1985-09-19 21:30:00. You may omit the time part.'),
            # preserve_default=True,
        ),
        migrations.AlterField(
            model_name='attraction',
            name='start_date',
            field=models.DateTimeField(verbose_name='start date', help_text='Please provide date and time according to the following example 1985-09-19 21:30:00. You may omit the time part.'),
            # preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='created_by',
            field=models.ForeignKey(related_name='events', to=settings.AUTH_USER_MODEL),
            # preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='end_date',
            field=models.DateTimeField(blank=True, null=True, verbose_name='end date of the event', help_text='Please provide date and time according to the following example 1985-09-19 21:30:00. You may omit the time part.'),
            # preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='start_date',
            field=models.DateTimeField(blank=True, null=True, verbose_name='start date of the event', help_text='Please provide date and time according to the following example 1985-09-19 21:30:00. You may omit the time part.'),
            # preserve_default=True,
        ),
    ]
