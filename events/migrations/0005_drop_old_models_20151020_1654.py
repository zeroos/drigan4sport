# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db import transaction
import reversion


def remove_old_entities(state, editor):
    def delete(model):
        for e in state.get_model(model).objects.filter(deleted_at__isnull=False):
            with reversion.create_revision(), transaction.atomic():
                e.delete()

    delete('events.attraction')
    delete('events.event')

class Migration(migrations.Migration):

    dependencies = [
        ('events', '0004_auto_20150521_1225'),
    ]

    operations = [
        migrations.RunPython(remove_old_entities, lambda *args: None),
    ]
