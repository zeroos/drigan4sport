# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def migrate_categories(apps, schema_editor):
    Category = apps.get_model('events', 'Category')
    OldCategory = apps.get_model('events', 'AttractionCategory')
    AttractionsCategoriesPtr = apps.get_model('events', 'SportCategory')
    for old_category in OldCategory.objects.all():
        new_category = Category()
        new_category.id = old_category.id
        new_category.name = old_category.name
        new_category.slug = old_category.slug
        new_category.save()
    for attr_ptr in AttractionsCategoriesPtr.objects.all():
        cat = Category.objects.get(id=attr_ptr.attractioncategory_ptr.id)
        cat.attraction_only = True
        cat.save()


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0006_auto_20151224_1518'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('name', models.CharField(verbose_name='name', max_length=100)),
                ('slug', models.SlugField(verbose_name='slug')),
                ('attraction_only', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name_plural': 'attraction categories',
            },
            bases=(models.Model,),
        ),
        migrations.RunPython(migrate_categories),
        migrations.AlterField(
            model_name='attraction',
            name='category',
            field=models.ForeignKey(verbose_name='category', to='events.Category'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='category',
            field=models.ForeignKey(verbose_name='category', to='events.Category'),
            preserve_default=True,
        ),
        migrations.RemoveField(
            model_name='attractioncategory',
            name='parent',
        ),
        migrations.RemoveField(
            model_name='sportcategory',
            name='attractioncategory_ptr',
        ),
        migrations.RunSQL(
            'DROP TABLE IF EXISTS events_sportcategory;',
            state_operations=[
                migrations.DeleteModel(
                    name='SportCategory',)]
        ),
        migrations.RunSQL(
            'DROP TABLE IF EXISTS events_attractioncategory;',
            state_operations=[
                migrations.DeleteModel(
                    name='AttractionCategory',)]
        ),

    ]
