# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import migrations
from django.core.management.sql import emit_post_migrate_signal
import django


def create_permission_group(apps, schema_editor):
    Group = apps.get_model('auth', 'Group')
    User = apps.get_model('auth', 'User')
    Permission = apps.get_model('auth', 'Permission')

    db_alias = schema_editor.connection.alias
    if django.get_version() < '1.9.0':
        emit_post_migrate_signal(2, False, 'default', db_alias)
    else:
        emit_post_migrate_signal(2, False, db_alias)

    perm = Permission.objects.get(codename='add_event')
    group = Group(name="event_creators")
    group.save()
    group.permissions.add(perm)

    for user in User.objects.all():
        if perm in user.user_permissions.all():
            user.groups.add(group)
        else:
            for u_group in user.groups.all():
                if perm in u_group.permissions.all():
                    user.groups.add(group)
                    break


def reverse_create_permission_group(apps, schema_editor):
    Group = apps.get_model('auth', 'Group')
    group = Group.objects.get(name="event_creators")

    group.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0005_auto_20150426_1543'),
        ('auth', '0001_initial'),
        ('sites', '0001_initial')
    ]

    operations = [
        migrations.RunPython(create_permission_group, reverse_create_permission_group)
    ]

