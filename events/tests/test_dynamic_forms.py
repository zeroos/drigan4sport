# -*- coding: utf-8 -*-
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.test import override_settings
from django.test.client import Client
from django.test.testcases import TestCase
from django.utils.text import slugify
from dynamic_forms.models import DynamicFormData

from events.tests.event_test_mixins import (SetupTestEventMixin,
                                            SetupTestDynamicFormMixin)
import json

@override_settings(
    LANGUAGE_CODE='en'
)
class TestParticipantsList(SetupTestEventMixin, TestCase,
                           SetupTestDynamicFormMixin):

    def setUp(self):
        self.setup_test_event()
        self.setup_test_dynamic_form()
        self.client = Client()

        ct = ContentType.objects.get(model="attraction")
        self.dynamic_form.content_type = ct
        self.dynamic_form.object_id = self.attraction2.id
        self.dynamic_form.save()

    def testParticipantsWithoutForm(self):
        url = reverse("participants-list",
                      kwargs={
                          "attraction_id": self.attraction.id,
                          "dynamic_form_id": 10
                      })
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

        url = reverse("participants-list",
                      kwargs={
                          "attraction_id": self.attraction.id,
                          "dynamic_form_id": self.dynamic_form.id
                      })
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def testParticipantsFormEmpty(self):
        self.assertTrue(self.client.login(username='fred1', password='secret'))
        url = reverse("participants-list",
                      kwargs={
                          "attraction_id": self.attraction2.id,
                          "dynamic_form_id": self.dynamic_form.id
                      })
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertIn(b"no participants", response.content)

    def testAddPorticipant(self):
        """ Already tested in TestFillForm class. """
        pass


class TestFillForm(TestCase, SetupTestEventMixin, SetupTestDynamicFormMixin):


    fixtures = ['sports_categories.json']

    def setUp(self):
        self.setup_test_event()
        self.setup_test_dynamic_form()
        self.client = Client()
        self.assertTrue(self.client.login(username='fred1', password='secret'))

        ct = ContentType.objects.get(model="attraction")
        self.dynamic_form.content_type = ct
        self.dynamic_form.object_id = self.attraction.id
        self.dynamic_form.can_be_filled = True
        self.dynamic_form.save()

        self.dynamic_form2.content_type = ct
        self.dynamic_form2.object_id = self.attraction2.id
        self.dynamic_form2.can_be_filled = False
        self.dynamic_form2.save()

        self.fill_form_url = reverse("fill-form",
                                     kwargs={
                                         "attraction_id": self.attraction.id,
                                         "dynamic_form_id": self.dynamic_form.pk
                                     })
        self.fill_form_url2 = reverse("fill-form",
                                      kwargs={
                                          "attraction_id": self.attraction2.id,
                                          "dynamic_form_id": self.dynamic_form2.pk
                                      })

        self.participants_url = reverse(
            "participants-list",
            kwargs={
                "attraction_id": self.attraction.id,
                "dynamic_form_id": self.dynamic_form.pk
            })

    def test_fill_ok(self):
        self.assertEqual(DynamicFormData.objects.all().count(), 0)
        response = self.client.post(
            self.fill_form_url,
            data={
                slugify("First"): "21342123",
                slugify("Second"): "SelectedOption",
                slugify("Third"): "0"
            }
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(DynamicFormData.objects.all().count(), 1)
        data = DynamicFormData.objects.all()[0].data
        self.assertEqual(data['First'], "21342123")
        self.assertEqual(data['Second'], 'SelectedOption')
        self.assertEqual(data['Third'], '0')

    def __make_bad_request_test(self, data):
        self.assertEqual(DynamicFormData.objects.all().count(), 0)
        response = self.client.post(
            self.fill_form_url,
            data=data
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(DynamicFormData.objects.all().count(), 0)

    def test_fill_bad_type(self):
        self.__make_bad_request_test({
            slugify("First"): "dupa",
            slugify("Second"): "Das",
            slugify("Third"): "0"
        })

    def test_fill_miss_required(self):
        self.__make_bad_request_test({
            slugify("First"): "dupa",
            slugify("Third"): "0"
        })

    def test_fill_invalid_choice(self):
        self.__make_bad_request_test({
            slugify("First"): "dupa",
            slugify("Second"): "Das",
            slugify("Third"): "NoSuchChoice"
        })

    def test_participants(self):
        post_response = self.client.post(
            self.fill_form_url,
            data={
                slugify("First"): "21342123",
                slugify("Second"): "SelectedOption",
                slugify("Third"): "0"
            }
        )

        self.assertEqual(post_response.status_code, 302)

        response = self.client.get(self.participants_url)

        self.assertIn(b"21342123", response.content)
        self.assertIn(b"SelectedOption", response.content)
        self.assertIn(b"0", response.content)

    def test_delete_participant(self):
        self.test_fill_ok()
        self.assertEqual(DynamicFormData.objects.all().count(), 1)
        dfd = DynamicFormData.objects.all()[0]

        response = self.client.post(reverse("dynamic_forms.views.manage_participant"),
                                    {'participant': dfd.id, 'method': 'delete', },
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(DynamicFormData.objects.all().count(), 0)

    def test_delete_participant_forbidden(self):
        self.test_fill_ok()
        self.assertEqual(DynamicFormData.objects.all().count(), 1)
        dfd = DynamicFormData.objects.all()[0]
        response = Client().post(reverse("dynamic_forms.views.manage_participant"),
                                 {'participant': dfd.id, 'method': 'delete', },
                                 HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        self.assertEqual(response.status_code, 302)
        self.assertEqual(DynamicFormData.objects.all().count(), 1)

    def test_confirm_participant(self):
        self.test_fill_ok()
        self.assertEqual(DynamicFormData.objects.all().count(), 1)
        dfd = DynamicFormData.objects.all()[0]
        self.assertEqual(dfd.confirmed, False)

        response = self.client.post(reverse("dynamic_forms.views.manage_participant"),
                                    {'participant': dfd.id, 'method': 'confirm', },
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(DynamicFormData.objects.all()[0].confirmed, True)

    def test_confirm_participant_forbidden(self):
        self.test_fill_ok()
        self.assertEqual(DynamicFormData.objects.all().count(), 1)
        dfd = DynamicFormData.objects.all()[0]
        self.assertEqual(dfd.confirmed, False)

        response = Client().post(reverse("dynamic_forms.views.manage_participant"),
                                    {'participant': dfd.id, 'method': 'confirm', },
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        self.assertEqual(response.status_code, 302)
        self.assertEqual(DynamicFormData.objects.all()[0].confirmed, False)

    def test_registration_closed(self):
        self.assertEqual(DynamicFormData.objects.filter(
            form=self.dynamic_form2).count(), 0)
        response = self.client.post(
            self.fill_form_url2,
            data={
                "First": "bum"
            }

        )

        self.assertEqual(response.status_code, 403)
        self.assertEqual(DynamicFormData.objects.filter(
            form=self.dynamic_form2).count(), 0)
