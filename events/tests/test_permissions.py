from django.core.urlresolvers import reverse
from django.test import TestCase
from django.test.utils import override_settings

from events.tests.event_test_mixins import SetupTestEventMixin, AccessTestMixin


class TestPermissionsOnObjectsCreatedByAPI(
    SetupTestEventMixin, TestCase, AccessTestMixin):
    """
    This TestCase tests all views in this module for correct permissions
    handling (i.e. returning 403 on pages that the user has no access to
    and redirecting to login page when it's needed.
    """


    def setUp(self):
        self.setup_test_event()

    def test_event_details_view(self):
        """
        All users should be able to see the event.
        """
        self.assertAccessAllowed(
            reverse('events-event-details', args=(self.event.id,)))
        self.assertAccessAllowed(
            reverse('events-event-details',args=(self.event.id,)), self.user2_data)
        self.assertAccessAllowed(
            reverse('events-event-details', args=(self.event.id,)), self.user1_data)

    @override_settings(DRIGAN_CREATE_EVENTS_WITHOUT_PERMISSION=False)
    def test_add_event_view_only_with_permission(self):
        """
        If DRIGAN_CREATE_EVENTS_WITHOUT_PERMISSION setting is False,
        only logged in users with 'add_event' permission should be able
        to see add event.
        """
        self.assertAccessDenied(reverse('events-add-event'))
        self.assertAccessAllowed(reverse('events-add-event'), self.user1_data)
        self.assertIsNotAuthorized(reverse('events-add-event'), self.user2_data)

    @override_settings(DRIGAN_CREATE_EVENTS_WITHOUT_PERMISSION=True)
    def test_add_event_view_without_permission(self):
        """
        If DRIGAN_CREATE_EVENTS_WITHOUT_PERMISSION setting is True,
        every logged in user can add event.
        """
        self.assertAccessDenied(reverse('events-add-event'))
        self.assertAccessAllowed(reverse('events-add-event'), self.user1_data)
        self.assertAccessAllowed(reverse('events-add-event'), self.user2_data)

    def test_edit_admins(self):
        """
        Only event admin or owner can see admins
        """
        self.assertAccessDenied(reverse('events-edit-admins',
                                        args=(self.event.id,)))
        self.assertGETAccessAllowed(reverse('events-edit-admins',
                                        args=(self.event.id,)), self.user1_data)
        self.assertAccessDenied(reverse('events-edit-admins',
                                        args=(self.event.id,)), self.user2_data)

    def test_add_delete_admins(self):
        """
        Check if permissions are working properly
        """

        """Only event creator has POST permissions here!"""
        self.assertAccessDenied(reverse('events-add-admins',
                                        args=(self.event.id, )), self.user2_data)
        self.assertAccessDenied(reverse('events-delete-admins',
                                        kwargs={'event_id': self.event.id, 'pk': self.user1.id}), self.user2_data)

        """
        1) Deny user addition if no POST data
        2) Add user2 as admin
        3) Check if he has GET admin permissions
        4) Remove him
        """
        self.assertPOSTAccessDenied(reverse('events-add-admins',
                                        args=(self.event.id, )), self.user1_data,
                                    customRedirect=reverse('events-edit-admins', args=(self.event.id, )))
        self.assertPOSTAccessAllowed(reverse('events-add-admins',
                                        args=(self.event.id,)), self.user1_data,
                                     {'new-admin-mail': self.user2_data['email']})
        self.assertGETAccessAllowed(reverse('events-edit-admins',
                                        args=(self.event.id,)), self.user2_data)
        self.assertPOSTAccessAllowed(reverse('events-delete-admins',
                                        kwargs={'event_id': self.event.id, 'pk': self.user2.id}), self.user1_data)

    def test_edit_event_view(self):
        """
        Only event owner can edit event.
        """

        self.assertAccessDenied(reverse('events-edit-event',
                                        args=(self.event.id,)))
        self.assertAccessAllowed(reverse('events-edit-event',
                                         args=(self.event.id,)), self.user1_data)
        self.assertAccessDenied(reverse('events-edit-event',
                                        args=(self.event.id,)), self.user2_data)

    def test_delete_event_view(self):
        """
        Only event owner can delete event.
        """

        self.assertAccessDenied(reverse('events-delete-event',
                                        args=(self.event.id,)))
        self.assertAccessDenied(reverse('events-delete-event',
                                        args=(self.event.id,)), self.user2_data)
        self.assertPOSTAccessAllowed(
            reverse('events-delete-event', args=(self.event.id,)), self.user1_data)

    def test_change_event_logo_view(self):
        """
        Only event owner can change event logo.
        """

        self.assertAccessDenied(reverse('events-change-event-logo',
                                        args=(self.event.id,)))
        self.assertAccessDenied(reverse('events-change-event-logo',
                                        args=(self.event.id,)), self.user2_data)
        self.assertPOSTAccessAllowed(
            reverse('events-change-event-logo',
            args=(self.event.id,)), self.user1_data)

    def test_change_attraction_logo_view(self):
        """
        Only event owner can change event attraction logo.
        """

        self.assertAccessDenied(
            reverse('events-change-attraction-logo', args=(self.attraction.id,)))
        self.assertAccessDenied(
            reverse('events-change-attraction-logo', args=(self.attraction.id,)),
            self.user2_data)
        self.assertPOSTAccessAllowed(
            reverse('events-change-attraction-logo', args=(self.attraction.id,)),
            self.user1_data)

    def test_add_attraction_view(self):
        """
        Only event owner can add attractions.
        """

        self.assertAccessDenied(
            reverse('events-add-attraction', args=(self.event.id,)))
        self.assertAccessDenied(
            reverse('events-add-attraction',
                    args=(self.event.id,)), self.user2_data)
        self.assertAccessAllowed(
            reverse('events-add-attraction', args=(self.event.id,)),
                    self.user1_data)

    def test_attraction_details_view(self):
        """
        All users should be able to see the attraction.
        """
        self.assertAccessAllowed(reverse('events-attraction-details',
                                         args=(self.attraction.id,)))
        self.assertAccessAllowed(
            reverse('events-attraction-details', args=(self.attraction.id,)),
            self.user2_data)
        self.assertAccessAllowed(
            reverse('events-attraction-details', args=(self.attraction.id,)),
            self.user1_data)

    def test_edit_attraction_view(self):
        """
        Only event owner can edit attractions.
        """

        self.assertAccessDenied(
            reverse('events-edit-attraction',
                    args=(self.event.id, self.attraction.id,)))
        self.assertAccessDenied(
            reverse('events-edit-attraction',
                    args=(self.event.id, self.attraction.id,)), self.user2_data)
        self.assertAccessAllowed(
            reverse('events-edit-attraction',
                    args=(self.event.id, self.attraction.id,)), self.user1_data)

    def test_delete_attraction_view(self):
        """
        Only event owner can delete attractions.
        """

        self.assertAccessDenied(
            reverse('events-delete-attraction',
                    args=(self.event.id, self.attraction.id,)))
        self.assertAccessDenied(
            reverse('events-delete-attraction',
                    args=(self.event.id, self.attraction.id,)), self.user2_data)
        self.assertPOSTAccessAllowed(
            reverse('events-delete-attraction',
                    args=(self.event.id, self.attraction.id,)), self.user1_data)


class TestPermissionsOnObjectsCreatedByModels(TestPermissionsOnObjectsCreatedByAPI):

    def setUp(self):
        self.setup_test_event_via_models()