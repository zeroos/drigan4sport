import datetime
from django.contrib.auth.models import User, Permission
from django.contrib.contenttypes.models import ContentType
from django.conf import settings
from django.core.urlresolvers import reverse
from django.test import Client

from dynamic_forms.models import DynamicForm, DynamicFormField
from events.models import Attraction, Event, Category, Organizer


class SetupTestEventMixin(object):
    """
    This mixin creates an example Event with Attraction and saves them in
    `self.event` end `self.attraction` fields.
    """

    fixtures = ['sports_categories.json']

    def set_up_users(self):
        """
        Sets up two users and event with attraction for them:
            user1 -- creator of event1
            user2 -- just a normal user

        This has to be done through API, because we test, for example, if
        correct permissions have been assigned.
        """
        self.user1_data = {
            'username': 'fred1',
            'email': 'fred@example.com',
            'password': 'secret',
        }
        self.user2_data = {
            'username': 'john2',
            'email': 'john@example.com',
            'password': 'secret',
        }
        self.user3_data = {
            'username': 'mark3',
            'email': 'mark@example.com',
            'password': 'secret',
        }

        self.user1 = User.objects.create_user(**self.user1_data)
        self.user2 = User.objects.create_user(**self.user2_data)
        self.user3 = User.objects.create_user(**self.user3_data)

        content_type = ContentType.objects.get(app_label='events', model='event')
        add_event_perm = Permission.objects.get(name='Can add event',
                                                content_type=content_type,
                                                codename='add_event')
        User.objects.get(username=self.user1_data['username']).user_permissions.add(add_event_perm)

    def set_up_events_via_api(self):
        try:
            c = Client()
            c.login(**self.user1_data)

            #TODO: IMO nie powinniśmy się odnosić do rzeczy z fixtur za pomocą 
            # id. Tak mówyli na PyConie w piątek, więc to prawda :P

            c.post(reverse('events-add-event'), {
                   'event-name': 'event1',
                   'event-category': '7',
                   'organizer-name': 'johnny',
                   'organizer-mail': 'john@example.com',
                   'organizer-phone': '555555',
                   })

            self.event = Event.objects.get(name='event1')

            c = Client()
            c.login(**self.user1_data)
            c.post(reverse('events-add-attraction', args=(self.event.id,)), {
                'name': 'attraction1',
                'start_date': '2030-12-12',
                'end_date': '2031-12-12',
                'place': 'u-kingdom',
                'description': 'john@example.com',
                'category': '1'})

            self.attraction = Attraction.objects.get(name='attraction1')

            c = Client()
            c.login(**self.user1_data)
            c.post(reverse('events-add-attraction', args=(self.event.id,)), {
                'name': 'attraction2',
                'start_date': '2030-12-12',
                'end_date': '2031-12-12',
                'place': 'u-kingdom',
                'description': 'john@example.com',
                'category': '1'})

            self.attraction2 = Attraction.objects.get(name='attraction2')
        except (Event.DoesNotExist, Attraction.DoesNotExist) as e:
            raise ValueError(
                "Couldn't create objects, most probably there are some "
                "problems while loading category fixture ") from e

    def set_up_events_via_models(self):
        self.event = Event.objects.create(
            name="event1",
            category=Category.objects.filter(attraction_only=False)[0],
            created_by=self.user1,
            organizer=Organizer.objects.create(
                name="foo",
                mail="foo@bar.pl",
                phone="5555-555-55",
                related_user=self.user1
            )
        )

        self.attraction = Attraction.objects.create(
            event=self.event,
            name='attraction1',
            start_date=datetime.datetime(2030, 12, 12),
            end_date=datetime.datetime(2031, 12, 12),
            place="u-kingdom",
            description='foobar',
            category=Category.objects.all()[1]
        )

        self.attraction2 = Attraction.objects.create(
            event=self.event,
            name='attraction1',
            start_date=datetime.datetime(2030, 12, 12),
            end_date=datetime.datetime(2031, 12, 12),
            place="u-kingdom",
            description='foobar',
            category=Category.objects.all()[1]
        )

    def setup_test_event(self):
        """
        Sets up two users and event with attraction for them:
            user1 -- creator of event1
            user2 -- just a normal user

        This has to be done through API, because we test, for example, if
        correct permissions have been assigned.
        """

        self.set_up_users()
        self.set_up_events_via_api()

    def setup_test_event_via_models(self):

        self.set_up_users()
        self.set_up_events_via_models()


class SetupTestDynamicFormMixin(object):
    def setup_test_dynamic_form(self):
        self.dynamic_form = DynamicForm.objects.create()
        self.dynamic_form2 = DynamicForm.objects.create()

        self.dynamic_field_1 = DynamicFormField.objects.create(
            form=self.dynamic_form,
            field_type="DynamicIntegerField",
            name="First",
            required=False
        )
        self.dynamic_field_2 = DynamicFormField.objects.create(
            form=self.dynamic_form,
            field_type="DynamicCharField",
            name="Second",
            required=True
        )
        self.dynamic_field_3 = DynamicFormField.objects.create(
            form=self.dynamic_form,
            field_type="DynamicChoicesField",
            name="Third",
            required=False
        )

        self.dynamic_field_3.dynamic_field.add_choice(self.dynamic_field_3,
                                                      "foo")
        self.dynamic_field_3.dynamic_field.add_choice(self.dynamic_field_3,
                                                      "bar")
        self.dynamic_field_3.save()
        self.dynamic_form.save()

        self.dynamic_field_2_1 = DynamicFormField.objects.create(
            form=self.dynamic_form2,
            field_type="DynamicCharField",
            name="First",
            required=True
        )


class AccessTestMixin(object):
    """ This mixin provides some useful functions to test url access """

    def assertAccessAllowed(self, url, user=None):
        """
        Asserts the user has access to given url using both POST and GET

        :param str: a url to test
        :param dict user: function will log in as user with given credentials
         """
        self.assertGETAccessAllowed(url, user)
        self.assertPOSTAccessAllowed(url, user)

    def assertGETAccessAllowed(self, url, user=None):
        """
        Asserts the user has access to given url using GET method

        :param str: a url to test
        :param dict user: function will log in as user with given credentials
        """
        c = Client()
        if user is None:
            user = {}
        else:
            c.login(**user)
        response = c.get(url)

        error_msg = "User '{}' does not have GET access to '{}'".format(
            user.get("username", "guest"),
            url)
        self.assertEqual(response.status_code, 200, error_msg)

    def assertPOSTAccessAllowed(self, url, user=None, data=None):
        """
        Asserts the user has access to given url by POST method

        :param str: a url to test
        :param dict user: function will log in as user with given credentials
        :param data: POST data to be passed with request
        """
        c = Client()
        if user is None:
            user = {}
        else:
            c.login(**user)
        response = c.post(url, data)
        if response.status_code != 200:
            # if response is not 200 make sure that it redirects to url
            # different than login_url

            error_msg = "User '{}' does not have POST access to '{}'".format(
                user.get("username", "guest"),
                url)
            self.assertEqual(response.status_code, 302, error_msg)
            self.assertNotEqual(response.url,
                                settings.LOGIN_URL + "?next=" + url)

    def assertPOSTAccessDenied(self, url, user=None, data=None, customRedirect=None):
        """
        Asserts the user don't have access to given url by POST method

        :param str: a url to test
        :param dict user: function will log in as user with given credentials
        :param data: POST data to be passed with request
        :param customRedirect: use if your function redirects to unusual url - otherwise it can break assertion
        """

        c = Client()
        if user is None:
            user = {}
        else:
            c.login(**user)

        error_msg = "User '{}' does not have POST access to '{}'".format(
                user.get("username", "guest"),
                url)

        response = c.post(url, data)

        if customRedirect is None:
            customRedirect = url

        if response.status_code == 302:
            self.assertRedirects(response, customRedirect,
                                 msg_prefix=error_msg)
        else:
            self.assertIn(response.status_code, [403, 405])  # forbidden

    def assertAccessDenied(self, url, user=None):
        """
        Asserts the user doesn't have access to given url

        :param str: a url to test
        :param dict user: function will log in as user with given credentials
        """
        c = Client()
        if user is None:
            user = {}
        else:
            c.login(**user)
        response = c.get(url)

        error_msg = "User '{}' does have access to '{}'".format(
            user.get("username", "guest"),
            url)

        if response.status_code == 302:
            self.assertRedirects(response, settings.LOGIN_URL + "?next=" + url,
                                 msg_prefix=error_msg)
        else:
            self.assertIn(response.status_code, [403, 405])

        response = c.post(url)
        if response.status_code == 302:
            self.assertRedirects(response, settings.LOGIN_URL + "?next=" + url,
                                 msg_prefix=error_msg)
        else:
            self.assertIn(response.status_code, [403, 405])  # forbidden

    def assertIsNotAuthorized(self, url, user=None):
        """
        Asserts the user is not authorized to have access to given url

        :param str: a url to test
        :param dict user: function will log in as user with given credentials
        """
        c = Client()
        if user is None:
            user = {}
        else:
            c.login(**user)
        response = c.get(url)

        error_msg = "User '{}' does have access to '{}'".format(
            user.get("username", "guest"),
            url)

        if response.status_code == 302:
            self.assertRedirects(response, settings.LOGIN_URL + "?next=" + url,
                                 msg_prefix=error_msg)
        else:
            self.assertEqual(response.status_code, 401)  # unauthorized

        response = c.post(url)
        if response.status_code == 302:
            self.assertRedirects(response, settings.LOGIN_URL + "?next=" + url,
                                 msg_prefix=error_msg)
        else:
            self.assertEqual(response.status_code, 401)  # unauthorized
