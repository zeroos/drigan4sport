from django.core.urlresolvers import reverse
from django.test import TestCase, Client
from django.contrib.contenttypes.models import ContentType
from django.utils.text import slugify

from events.tests.event_test_mixins import (SetupTestEventMixin,
                                            SetupTestDynamicFormMixin,
                                            AccessTestMixin)
from events.models import LIST_VISIBLE_FOR_ORGANIZER
from dynamic_forms.models import DynamicForm


class ListVisibilityTest(SetupTestEventMixin, TestCase,
                         SetupTestDynamicFormMixin,
                         AccessTestMixin):
    """
    Tests if the feature to change participants list visibility works correctly.
    """

    def setUp(self):
        self.setup_test_event()
        self.setup_test_dynamic_form()
        self.client = Client()
        self.assertTrue(self.client.login(username='fred1', password='secret'))

        ct = ContentType.objects.get(model="attraction")
        self.dynamic_form.content_type = ct
        self.dynamic_form.object_id = self.attraction.id
        self.dynamic_form.can_be_filled = True
        self.dynamic_form.save()

        self.fill_form_url = reverse(
            "fill-form",
             kwargs={
                 "attraction_id": self.attraction.id,
                 "dynamic_form_id": self.dynamic_form.pk
             })

        self.participants_url = reverse(
            "participants-list",
            kwargs={
                "attraction_id": self.attraction.id,
                "dynamic_form_id": self.dynamic_form.pk
            })

        client = Client()
        self.assertTrue(client.login(**self.user3_data))
        client.post(
            self.fill_form_url,
            data={
                slugify("First"): "21342123",
                slugify("Second"): "SelectedOption",
                slugify("Third"): "0"
            }
        )

    def test_default_visibility(self):
        """
        By default the list should be only visible to the creator.
        """
        self.assertEqual(self.dynamic_form.list_visibility,
                         LIST_VISIBLE_FOR_ORGANIZER)

    def test_organizer_visibility(self):
        """
        When visibility is set to `LIST_VISIBLE_FOR_ORGANIZER` only organizer
        should see the list
        """
        self.dynamic_form.list_visibility = LIST_VISIBLE_FOR_ORGANIZER
        self.dynamic_form.save()
        self.assertAccessDenied(self.participants_url)
        self.assertGETAccessAllowed(self.participants_url, self.user1_data)
        self.assertAccessDenied(self.participants_url, self.user2_data)
        self.assertAccessDenied(self.participants_url, self.user3_data)

    def test_participants_visibility(self):
        """
        When visibility is set to `LIST_VILIST_VISIBLE_FOR_PARTICIPANTSSIBLE_FOR_PARTICIPANTS` every
        participant should be able to see the list
        """
        self.dynamic_form.list_visibility = \
            DynamicForm.LIST_VISIBLE_FOR_PARTICIPANTS
        self.dynamic_form.save()
        self.assertAccessDenied(self.participants_url)
        self.assertGETAccessAllowed(self.participants_url, self.user1_data)
        self.assertAccessDenied(self.participants_url, self.user2_data)
        self.assertGETAccessAllowed(self.participants_url, self.user3_data)

    def test_all_visibility(self):
        """
        When visibility is set to `LIST_VISIBLE_FOR_ALL` everyone should
        be able to see the list
        """
        self.dynamic_form.list_visibility = DynamicForm.LIST_VISIBLE_FOR_ALL
        self.dynamic_form.save()
        self.assertGETAccessAllowed(self.participants_url)
        self.assertGETAccessAllowed(self.participants_url, self.user2_data)
        self.assertGETAccessAllowed(self.participants_url, self.user1_data)
        self.assertGETAccessAllowed(self.participants_url, self.user3_data)
