from events.models import Event, Attraction, Organizer, Category, SerialEventGroup
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from reversion.admin import VersionAdmin


class EventAdmin(VersionAdmin):

    list_display = ('name', 'edition', 'organizer_name', 'start_date', 'end_date',
                    'website', 'creator_name', 'creators_mail')
    search_fields = ('name', 'organizer__name', 'created_by__username')
    list_filter = ('start_date', 'end_date')

    def organizer_name(self, obj):
        return obj.organizer.name
    organizer_name.short_description = _('organizer')
    organizer_name.admin_order_field = 'organizer__name'

    def creator_name(self, obj):
        return obj.created_by.username
    creator_name.short_description = _('creator')
    creator_name.admin_order_field = 'created_by__username'

    def creators_mail(self, obj):
        return obj.created_by.email
    creators_mail.short_description = _('creator\'s mail')
    creators_mail.admin_order_field = 'created_by__email'


class OrganizerAdmin(VersionAdmin):

    list_display = ('name', 'mail', 'phone', 'related_username', 'related_usermail')
    search_fields = ('name', 'related_user__username')

    def related_username(self, obj):
        return obj.related_user.username
    related_username.short_description = _('creator')
    related_username.admin_order_field = 'related_user__username'

    def related_usermail(self, obj):
        return obj.related_user.email
    related_usermail.short_description = _('creator\'s mail')
    related_usermail.admin_order_field = 'related_user__email'


class AttractionAdmin(VersionAdmin):

    list_display = ('name', 'place', 'description', 'category', 'event_name')
    list_filter = ('start_date', 'end_date')
    search_fields = ('name', 'event__name')

    def event_name(self, obj):
        return obj.event.name
    event_name.short_description = _('event')
    event_name.admin_order_field = 'event__name'


class CategoryAdmin(VersionAdmin):

    list_display = ('name', 'slug', 'attraction_only')
    list_filter = ('attraction_only',)


admin.site.register(Event, EventAdmin)
admin.site.register(Attraction, AttractionAdmin)
admin.site.register(Organizer, OrganizerAdmin)
admin.site.register(SerialEventGroup)
admin.site.register(Category, CategoryAdmin)
