from django.shortcuts import render_to_response
from django.template import RequestContext
from django import http
from django.db import transaction
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext as _
from django.views.generic import DeleteView
from django.views.generic.detail import DetailView
from events.forms import AddOrganizerForm, EditOrganizerForm, \
    AddEventForm, EditEventForm, AddAttractionForm, ChangeEventLogoForm
from events.models import Event, Attraction, Category
from guardian.decorators import permission_required
from guardian.shortcuts import assign_perm, remove_perm, get_users_with_perms
from django.conf import settings
from reversion import revisions as reversion
import logging


logger = logging.getLogger('drigan_access_and_modification')


ACTION_TYPES = {'access': 'VIEWED',
                'addition': 'ADDED',
                'edition': 'EDITED',
                'deletion': 'DELETED'}


def log_action(obj, action_type, user, message=''):
    logger.info(message, extra={'object_type': obj,
                                'object_name': obj.name,
                                'object_pk': obj.id,
                                'action_type': ACTION_TYPES[action_type],
                                'user': user})


def event_details(request, event_id):
    event = get_object_or_404(Event, id=event_id)
    change_logo_form = ChangeEventLogoForm()
    return render_to_response(
        "events/event_details.html",
        {
            "event": event,
            "change_logo_form": change_logo_form,
        }, context_instance=RequestContext(request))


@login_required
@transaction.atomic
def add_event(request):
    if not (request.user.has_perm('events.add_event') or
            settings.DRIGAN_CREATE_EVENTS_WITHOUT_PERMISSION):
        return http.HttpResponse('401 Unauthorized', status=401)
    if request.method == 'POST':
        form = AddEventForm(request.POST, prefix='event')
        organizer_form = AddOrganizerForm(request.POST, prefix='organizer')
        if form.is_valid():
            event = form.save(commit=False)
            event.created_by = request.user
            if organizer_form.is_valid():
                organizer = organizer_form.save(commit=False)
                organizer.related_user = request.user
                organizer.save()
                event.organizer = organizer
                event.save()
                messages.success(
                    request, _('Event has been added successfully.'))
                log_action(event, 'addition', request.user)
                log_action(organizer, 'addition', request.user)
                return http.HttpResponseRedirect(reverse(
                    'events.views.event_details',
                    args=(event.id,)))
    else:
        form = AddEventForm(prefix='event')
        form.fields['category'].queryset = Category.objects.filter(attraction_only=False)
        organizer_form = AddOrganizerForm(prefix='organizer')
    return render_to_response("events/event_add.html",
                              {"form": form, "organizer_form": organizer_form},
                              context_instance=RequestContext(request))


@login_required
@permission_required('events.change_event', (Event, 'id', 'event_id'))
@transaction.atomic
@reversion.create_revision()
def edit_event(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    organizer = event.organizer
    if request.method == "POST":
        form = EditEventForm(request.POST, instance=event, prefix='event')
        organizer_form = EditOrganizerForm(
            request.POST, instance=organizer, prefix='organizer')
        if form.is_valid() and organizer_form.is_valid():
            event = form.save()
            organizer = organizer_form.save()
            messages.success(request, _('Event has been changed successfully.'))
            log_action(event, 'edition', request.user)
            log_action(organizer, 'edition', request.user)
            return http.HttpResponseRedirect(reverse(
                'events.views.event_details',
                args=(event.id,)))
    else:
        form = EditEventForm(instance=event, prefix='event')
        form.fields['category'].queryset = Category.objects.filter(attraction_only=False)
        organizer_form = EditOrganizerForm(instance=organizer, prefix='organizer')

    return render_to_response(
        "events/event_edit.html",
        {
            "form": form,
            "organizer_form": organizer_form,
            "event": event
        },
        context_instance=RequestContext(request))


def can_edit_admins(self, user):
    return user.id == self.created_by.id or user.is_superuser
Event.can_edit_admins = can_edit_admins


class AdminsView(DetailView):
    model = Event
    template_name = "events/event_admin.html"
    pk_url_kwarg = "event_id"

    @method_decorator(login_required())
    @method_decorator(permission_required('events.change_event', (Event, 'id', 'event_id')))
    def dispatch(self, *args, **kwargs):
        return super(AdminsView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AdminsView, self).get_context_data(**kwargs)
        event = self.get_object()
        context['admins'] = event.get_admins()
        context['event'] = event
        context['can_edit_admins'] = event.can_edit_admins(self.request.user)
        return context

    @method_decorator(login_required())
    @method_decorator(permission_required('events.change_event', (Event, 'id', 'event_id')))
    def post(self, request, *args, **kwargs):
        event = self.get_object()
        if event.can_edit_admins(request.user):
            if "add" in request.path:
                mail = request.POST.get("new-admin-mail")

                queryset = User.objects.filter(email=mail)
                if queryset.count() != 1 or not bool(mail):
                    messages.error(request, _('There is no user with such email address'))
                else:
                    assign_perm("events.change_event", queryset.first(), event)
                    messages.success(request, _('Admin added successfully'))
                return http.HttpResponseRedirect(
                    reverse('events-edit-admins', args=(event.id,)))

            elif "delete" in request.path:
                user = User.objects.get(id=self.kwargs["pk"])
                remove_perm("events.change_event", user, event)
                messages.success(request, _('Owner deleted successfully'))
                return http.HttpResponseRedirect(
                    reverse('events-edit-admins',
                            args=(event.id,)))

        else:
            messages.error(request, _('Only event creator can add/remove admin'))
            return http.HttpResponseRedirect(
                reverse('events-edit-admins',
                        args=(event.id,)))


class DeleteEventView(DeleteView):

    model = Event

    success_url = reverse_lazy("index")

    @method_decorator(login_required)
    @method_decorator(permission_required('events.delete_event', (Event, 'id', 'pk')))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    @method_decorator(login_required)
    @method_decorator(permission_required('events.delete_event', (Event, 'id', 'pk')))
    def delete(self, request, *args, **kwargs):

        event = self.get_object()

        dependent_objects = event.attractions.all()
        log_message = ''
        if dependent_objects:
            dependent_objects_str = ', '.join([obj.name for obj in dependent_objects])
            log_message = 'Dependent attractions have been removed: {}. '.format(
                dependent_objects_str)

        result = super().delete(request, *args, **kwargs)
        log_action(event, 'deletion', request.user, log_message)
        messages.success(request, _('Event has been deleted.'))
        return result


class DeleteAttractionView(DeleteView):

    model = Attraction

    @method_decorator(login_required)
    @method_decorator(permission_required('events.delete_attraction', (Attraction, 'id', 'pk')))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    @method_decorator(login_required)
    @method_decorator(permission_required('events.delete_event', (Attraction, 'id', 'pk')))
    def delete(self, request, *args, **kwargs):

        attraction = self.get_object()

        result = super().delete(request, *args, **kwargs)
        log_action(attraction, 'deletion', request.user)
        messages.success(request, _('Attraction has been deleted.'))
        return result

    def get_success_url(self):
        return reverse("events-event-details", kwargs={'event_id': self.object.event.pk})


@login_required
@permission_required('events.change_event', (Event, 'id', 'object_id'))
@reversion.create_revision()
def change_event_logo(request, object_id):
    return change_logo(request, object_id, Event, event_details)


@login_required
@permission_required('events.change_attraction', (Attraction, 'id', 'object_id'))
@reversion.create_revision()
def change_attraction_logo(request, object_id):
    return change_logo(request, object_id, Attraction, attraction_details)


@login_required
def change_logo(request, object_id, model_cls, reverse_view):
    if request.method == "POST":
        obj = get_object_or_404(model_cls, pk=object_id)
        form = ChangeEventLogoForm(request.POST, request.FILES,
                                   instance=obj)
        if form.is_valid():
            form.save()
            log_action(obj, 'edition', request.user, 'Logo changed')
            messages.success(request, _('Logo changed successfully'))
        else:
            messages.error(request, _('Form data incorrect.'))
            messages.error(request, form.errors.as_text())
    else:
        messages.error(request,
                       _('Logo not changed - no data given.'))
    return http.HttpResponseRedirect(reverse(reverse_view, args=(object_id,)))


@login_required
@permission_required('events.change_event', (Event, 'id', 'event_id'))
def add_attraction(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    if request.method == 'POST':
        form = AddAttractionForm(request.POST)
        if form.is_valid():
            attraction = form.save(commit=False)
            attraction.event = event
            attraction.save()
            form.save()
            messages.success(
                request, _('Attraction has been added successfully.'))
            log_action(attraction, 'addition', request.user)
            return http.HttpResponseRedirect(
                reverse('events.views.attraction_details',
                        args=(attraction.id,)))
    else:
        form = AddAttractionForm()
    return render_to_response("events/attraction_add.html",
                              {"form": form},
                              context_instance=RequestContext(request))


def attraction_details(request, attraction_id):
    attraction = get_object_or_404(Attraction, id=attraction_id)
    change_logo_form = ChangeEventLogoForm()

    attraction_forms_data = [
        {
            "form": f, "can_edit": f.can_edit_dynamic_form(request.user),
            "can_see_participants_list": f.can_see_participants_list(request.user)
        } for f in attraction.forms]

    return render_to_response(
        "events/attraction_details.html",
        {
            "attraction": attraction,
            "change_logo_form": change_logo_form,
            "attraction_forms_data": attraction_forms_data,
        }, context_instance=RequestContext(request))


@login_required
@permission_required('events.change_attraction', (Attraction, 'id', 'attraction_id'))
@reversion.create_revision()
def edit_attraction(request, event_id, attraction_id):
    attraction = get_object_or_404(Attraction, pk=attraction_id)
    if request.method == "POST":
        form = AddAttractionForm(request.POST, instance=attraction)
        if form.is_valid():
            attraction = form.save()
            messages.success(request,
                             _('Attraction has been changed successfully.'))
            log_action(attraction, 'edition', request.user)
            return http.HttpResponseRedirect(reverse(
                'events.views.attraction_details',
                args=(attraction.id,)))
    else:
        form = AddAttractionForm(instance=attraction)
    return render_to_response("events/attraction_edit.html",
                              {"form": form,
                               "attraction": attraction},
                              context_instance=RequestContext(request))
