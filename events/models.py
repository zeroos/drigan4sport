# -*- coding: utf-8 -*-
import datetime

from django.core.urlresolvers import reverse
from django.conf import settings
from django.db import models, transaction
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _

from guardian.shortcuts import assign_perm, get_users_with_perms

from dynamic_forms.models import DynamicForm
from dynamic_forms.list_visibility_restrictions import \
    default_access_participants_list_restrictions
import os.path

from reversion import revisions as reversion

LIST_VISIBLE_FOR_ORGANIZER = "organizer"


def get_datetime_help_text():
    format = settings.DATETIME_INPUT_FORMATS[0]
    totally_random_date = datetime.datetime(1985, 9, 19, 21, 30)
    message = _("""Please provide date and time according to the following example {example}. You may omit the time part.""").strip().format(
        format=format,
        example=totally_random_date.strftime(format))
    return message


def visible_for_organizer(user, form):
    """
    Restriction for DynamicForm's Participants List making it visible only for
    event organizer.
    """
    if user.is_authenticated():
        if (form.list_visibility == LIST_VISIBLE_FOR_ORGANIZER or
            form.list_visibility == DynamicForm.LIST_VISIBLE_FOR_PARTICIPANTS) and\
                form.content_object.event.organizer.related_user == user:
            return True
    return False


def visible_for_everyone_who_can_edit(user, form):
    return Attraction.DynamicFormMeta.can_edit(user, form)


class Category(models.Model):
    class Meta:
        verbose_name_plural = _('attraction categories')

    name = models.CharField(max_length=100, verbose_name=_('name'))
    slug = models.SlugField(verbose_name=_('slug'))
    attraction_only = models.BooleanField(default=False)

    def __str__(self):
        return self.name


@reversion.register()
class Organizer(models.Model):
    class Meta:
        verbose_name = _('organizer')
        verbose_name_plural = _('organizers')

    name = models.CharField(_("name"), max_length=200)
    mail = models.EmailField(_("e-mail address"))
    phone = models.CharField(_("phone number"), max_length=20)
    address = models.TextField(_("address"), blank=True, null=True)
    related_user = models.ForeignKey(User, null=False, blank=False)

    def __unicode__(self):
        return self.name


class SerialEventGroup(models.Model):
    name = models.CharField(_("name"), max_length=200)

    def __unicode__(self):
        return self.name


@reversion.register()
class Event(models.Model):

    class Meta:
        verbose_name = _('event')
        verbose_name_plural = _('events')

    name = models.CharField(_("event name"), max_length=200,
                            help_text=_("Without edition (eg. \"Long Race\", "
                                        "not \"Long Race 2013\")"))

    start_date = models.DateTimeField(
        _("start date of the event"),
        null=True, blank=True,
        help_text=get_datetime_help_text())

    end_date = models.DateTimeField(
        _("end date of the event"),
        null=True, blank=True,
        help_text=get_datetime_help_text())

    logo = models.ImageField(upload_to='uploads/events/logos',
                             null=True, blank=True)

    website = models.URLField(_("website"), max_length=100, blank=True)

    # TODO: Rename to owner
    created_by = models.ForeignKey(User, related_name="events")
    category = models.ForeignKey(Category,
                                 verbose_name=_('category'))
    description = models.TextField(_("description of the event"),
                                   blank=True, null=True)
    organizer = models.ForeignKey(Organizer)
    edition = models.CharField(_("edition name"), max_length=100,
                               blank=True,
                               help_text=_("Only if event is cyclic (eg. "
                                           "\"1\", \"2014\")"))
    event_group = models.ForeignKey(SerialEventGroup, null=True, blank=True)

    def __str__(self):
        return self.name

    def get_logo(self):

        if self.logo:
            try:
                self.logo.open('rb')
            except:
                return None
            return self.logo
        else:
            return None

    def get_other_editions(self):
        if self.event_group:
            return self.event_group.event_set.all().exclude(
                edition=self.edition)

    def get_absolute_url(self):
        return reverse("events-event-details", args=(self.id,))

    @transaction.atomic()
    def save(self, raw=False, **kwargs):

        # TODO: To anyone wandering: this is a hack and a workaround.
        # If we change created_by user new user is assigned edition rights
        # while owner still retains these rights.
        # Probably this needs more thought but it works.

        # TODO: This relies on undocumented? behavious of passing raw to
        # save method.

        if raw:
            super().save(raw=raw, **kwargs)
            return

        with transaction.atomic():
            is_creating = self.pk is None
            old_user = None
            if not is_creating:
                try:
                    old_user = Event.objects\
                        .only("created_by").get(pk=self.pk).created_by
                except Event.DoesNotExist:
                    pass
                    # TODO: Happens during undelete, should patch sofdtelete to
                    # So it passes raw=True to save.
            super().save(**kwargs)

            if old_user != self.created_by:
                assign_perm('events.change_event', self.created_by, self)
                assign_perm('events.delete_event', self.created_by, self)

            for a in self.attractions.all():
                assign_perm('events.change_attraction', self.created_by, a)
                assign_perm('events.delete_attraction', self.created_by, a)

    def get_admins(self):
        return list(get_users_with_perms(self))

    @staticmethod
    def get_all_admins():
        users = []
        for event in Event.objects.all():
            templist = list(get_users_with_perms(event))
            users = list(set(users) | set(templist))
        return users


@reversion.register()
class Attraction(models.Model):

    event = models.ForeignKey(Event, related_name="attractions")
    name = models.CharField(_("name"), max_length=200)
    start_date = models.DateTimeField(
        _("start date"), help_text=get_datetime_help_text())
    end_date = models.DateTimeField(
        _("end date"), help_text=get_datetime_help_text())
    logo = models.ImageField(upload_to='uploads/events/logos',
                             null=True, blank=True)
    place = models.CharField(_("place"), max_length=200)
    description = models.TextField(
        _("description of the attraction"), null=True, blank=True)
    category = models.ForeignKey(Category,
                                 verbose_name=_('category'))
    registered_only = models.BooleanField(_("Registered users only"), default=False)
    confirm_registration = models.BooleanField(_("Confirm user registration"), default=False)

    def __str__(self):
        return self.name

    def get_logo(self):
        if self.logo:
            return self.logo
        else:
            return None

    @property
    def forms(self):
        """ Returns a list off all forms related to this attraction """
        content_type = ContentType.objects.get_for_model(self)
        forms = DynamicForm.objects.filter(object_id=self.id,
                                           content_type=content_type)
        return forms

    def get_absolute_url(self):
        return reverse("events-attraction-details", args=(self.id,))

    def save(self, **kwargs):
        with transaction.atomic():
            super().save(**kwargs)
            assign_perm('events.change_attraction', self.event.created_by, self)
            assign_perm('events.delete_attraction', self.event.created_by, self)

    def get_admins(self):
        return list(get_users_with_perms(self))

    @staticmethod
    def get_all_admins():
        users = []
        for attraction in Attraction.objects.all():
            templist = list(get_users_with_perms(attraction))
            users = list(set(users) | set(templist))
        return users

    class Meta:
        ordering = ['start_date', 'name']
        verbose_name = _('attraction')
        verbose_name_plural = _('attractions')

    class DynamicFormMeta:

        @staticmethod
        def can_edit(user, form):
            return user.has_perm("change_attraction", form.content_object)

        list_visibility_choices = (
            (DynamicForm.LIST_VISIBLE_FOR_ALL, _("everyone")),
            (LIST_VISIBLE_FOR_ORGANIZER, _("organizer only")),
            (DynamicForm.LIST_VISIBLE_FOR_PARTICIPANTS, _("all participants")),
        )

        access_participants_list_restrictions = \
            default_access_participants_list_restrictions + \
            [visible_for_organizer, visible_for_everyone_who_can_edit]

        default_list_visibility = LIST_VISIBLE_FOR_ORGANIZER

        @staticmethod
        def can_save(user, form):
            if user == form.content_object.event.organizer.related_user:
                return True
            return False
