
from django.utils.translation import ugettext as _

from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.core.exceptions import PermissionDenied

from dynamic_forms.views import FillForm as GenericFillForm, BackRef
from dynamic_forms.views import ParticipantList as GenericParticipantList
from dynamic_forms.views import SaveList as GenericSaveList
from dynamic_forms.views import EditDynamicForm as BaseEditDynamicForm

from events.models import Attraction


class AttractionFormMixin(object):
    """
    Mixin that should be added to all views processing attraction forms.

    For this mixin to work properly it is assumed that the url has two
    parameters --` attraction_id` and `dynamic_form_id`.
    """
    def mixin_attraction_form(self, **kwargs):
        """
        Method that adds `self.attraction` and `self.form` fields. It's a good
        idea to use it at the beginning of `dispatch` method of CBV to have
        those fields available in all other methods.
        """
        self.attraction = get_object_or_404(Attraction,
                                            pk=self.kwargs['attraction_id'])
        form_id = int(self.kwargs['dynamic_form_id'])
        forms = [f for f in self.attraction.forms if f.id == form_id]
        if len(forms) == 0:
            raise Http404("Attraction does not contain a form with this id")
        assert len(forms) == 1
        self.form = forms[0]


class FillForm(GenericFillForm, AttractionFormMixin):
    """
    A view used to fill dynamic form related to a attraction. See
    AttractionFormMixin for some details about requirements.
    """
    template_name = "events/signup/form_fill.html"

    def dispatch(self, request, *args, **kwargs):
        self.mixin_attraction_form()
        if self.attraction.registered_only and not self.request.user.is_authenticated():
            raise PermissionDenied()
        return super(FillForm, self).dispatch(request, *args, **kwargs)

    def get_form_id(self):
        return self.form.id

    def get_context_data(self, **kwargs):
        context = super(FillForm, self).get_context_data(**kwargs)
        context['attraction'] = self.attraction
        return context


    def get_success_url(self):
        return reverse('events-attraction-details',
                       args=(self.attraction.id,))


class ParticipantList(GenericParticipantList, AttractionFormMixin):
    """
    A view that displays data collected from one of the forms related to
    a attraction. See AttractionFormMixin for some details about requirements.
    """

    template_name = "events/signup/list.html"

    def dispatch(self, request, *args, **kwargs):
        self.mixin_attraction_form()
        return super(ParticipantList, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ParticipantList, self).get_context_data(**kwargs)
        context['attraction'] = self.attraction
        context['form'] = self.form
        context['can_save_list'] = self.form.can_save_participants_list(
            self.request.user)
        return context


class SaveList(GenericSaveList, AttractionFormMixin):
    def dispatch(self, request, *args, **kwargs):
        self.mixin_attraction_form()
        if not self.form.can_save_participants_list(request.user):
            raise PermissionDenied()
        return super(SaveList, self).dispatch(request, *args, **kwargs)


class EditDynamicForm(BaseEditDynamicForm):

    def get_backref(self, dynamic_form):
        attraction = Attraction.objects.get(
            pk=self.dynamic_form.object_id
        )
        return BackRef(
            reverse(
                "events-attraction-details",
                kwargs={'attraction_id': attraction.pk}),
            _("Done editing form, return to attraction")
        )

    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)




