from django.conf.urls import patterns, url
from events import views, dynamic_forms_views

urlpatterns = patterns('',
    url(r'^(?P<event_id>\d+)/$', views.event_details,
        name='events-event-details'),
    url(r'^edit/(?P<event_id>\d+)/$', views.edit_event,
        name='events-edit-event'),
    url(r'^(?P<event_id>\d+)/admins/$', views.AdminsView.as_view(),
        name='events-edit-admins'),
    url(r'^(?P<event_id>\d+)/admins/delete/(?P<pk>\d+)/$', views.AdminsView.as_view(),
        name='events-delete-admins'),
    url(r'^(?P<event_id>\d+)/admins/add/$', views.AdminsView.as_view(),
        name='events-add-admins'),
    url(r'^delete/(?P<pk>\d+)/$', views.DeleteEventView.as_view(),
        name='events-delete-event'),
    url(r'^add/$', views.add_event,
        name='events-add-event'),
    url(r'^change_logo/(?P<object_id>\d+)/$',
        views.change_event_logo, name='events-change-event-logo'),
    url(r'^change_logo/attraction/(?P<object_id>\d+)/$',
        views.change_attraction_logo, name='events-change-attraction-logo'),
    url(r'^attractions/(?P<attraction_id>\d+)/$', views.attraction_details,
        name='events-attraction-details'),
    url(r'^(?P<event_id>\d+)/attractions/add/$', views.add_attraction,
        name='events-add-attraction'),
    url(r'^(?P<event_id>\d+)/attractions/edit/(?P<attraction_id>\d+)/$',
        views.edit_attraction, name='events-edit-attraction'),
    url(r'^(?P<event_id>\d+)/attractions/delete/(?P<pk>\d+)/$',
        views.DeleteAttractionView.as_view(), name='events-delete-attraction'),
    # dynamic forms
    url(r'^attractions/(?P<attraction_id>\d+)/fill/(?P<dynamic_form_id>\d+)/$',
        dynamic_forms_views.FillForm.as_view(),
        name='fill-form'),
    url(r'^attractions/(?P<attraction_id>\d+)/list/(?P<dynamic_form_id>\d+)/$',
        dynamic_forms_views.ParticipantList.as_view(),
        name='participants-list'),
    url(r'^attractions/(?P<attraction_id>\d+)/save/(?P<dynamic_form_id>\d+)' +
        r'(?:/format/(?P<format>\w+))?/?$',dynamic_forms_views.SaveList.as_view(),
        name='save-participants-list'),


)

dynform_url_overrides = patterns('',
    url(r'^edit/(?P<dynamic_form_id>\d+)/$',
        dynamic_forms_views.EditDynamicForm.as_view(),
        name="dynamic_forms.views.edit_dynamic_form"),
)