# -*- coding: utf-8 -*-

from django.core.exceptions import ImproperlyConfigured
import typing
import os

__XX_NODEFAULT = object()

T = typing.TypeVar('T')

__all__ = [
    'get_env', 'get_env_bool', 'get_string_list_env'
]

def get_env(
        var:str, default=__XX_NODEFAULT,
        converter:typing.Callable[[str], T]=lambda x:x) -> T:
    val = os.environ.get(var, __XX_NODEFAULT)
    if val is __XX_NODEFAULT:
        if default is __XX_NODEFAULT:
            raise ImproperlyConfigured("Enviorment variablke {} is required".format(var))
        return default
    return converter(val)

def get_string_list_env(var:str, default=__XX_NODEFAULT) -> list:
    def converter(val:str) -> list:
        return list(val.split(":"))
    return get_env(var, default=default, converter=converter)

def bool_converter(val:str) -> bool:

    # Copied from configparser module

    BOOLEAN_STATES = {
        '1': True, 'yes': True, 'true': True, 'on': True,
        '0': False, 'no': False, 'false': False, 'off': False
    }

    if val.lower() not in BOOLEAN_STATES:
        raise ValueError('Not a boolean: %s' % val)
    return BOOLEAN_STATES[val.lower()]

def get_env_bool(var,default=__XX_NODEFAULT) -> bool:
    return get_env(var, default, bool_converter)
