/**
 * I've had to quickly add this small funciton, so I made it in this ugly way.
 * We should make it more modular and minify this code - this file should be
 * generated automatically.
 */

$(function() {
    $('form.autosubmit').change(function(){
        $(this).submit();
    });
    $('form.autosubmit button[type="submit"]').hide();
});

/*
 * Hack to block submission button after it's clicked
 */

$('form.only-one-submit').submit(function(){
    $('button[type="submit"]').prop('disabled', true);     
});

//Functions to get AJAX work

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}