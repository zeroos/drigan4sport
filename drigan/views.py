from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from django.views.generic.list import ListView
from events.models import Event
from guardian.shortcuts import get_objects_for_user

from datetime import datetime


class StartView(ListView):

    template_name = "start.html"
    context_object_name = 'upcoming_events_list'
    def get_queryset(self):
        all_events = Event.objects.order_by('-start_date').filter(start_date__gt=datetime.now())
        return all_events

    def get_context_data(self, *args, **kwargs):
        context = super(StartView, self).get_context_data(*args, **kwargs)
        user = self.request.user
        if user.is_authenticated():
            my_events = get_objects_for_user(user, 'events.change_event', None, True, False, False) | user.events.all()
            my_events = my_events.order_by('-start_date')
            context['user_events_list'] = my_events
        return context
