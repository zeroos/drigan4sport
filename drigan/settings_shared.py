from django.contrib import messages

from .settings_utils import *

# Django settings for drigan project.

SITE_ID = 1

LANGUAGE_CODE = 'pl'

LANGUAGES = (
    ('pl', 'Polski'),
    ('en', 'English'),
)

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True


# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    # 'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
    "django.core.context_processors.static",
    "django.contrib.messages.context_processors.messages",
    "drigan.context_processors.events_settings",
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'reversion.middleware.RevisionMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',  # this is default
    'guardian.backends.ObjectPermissionBackend',
)

ANONYMOUS_USER_ID = -1

ROOT_URLCONF = 'drigan.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'drigan.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'registration',
    'drigan_registration',
    'events',
    'dynamic_forms',
    'drigan',
    'imagekit',
    'guardian',
    'django_hstore',
    'positions',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    'scout_fieldtypes',
    'localflavor',
    'reversion',
    'run_manager',
    'run_ranking',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'drigan': {
            'format': '%(levelname)s %(asctime)s %(object_type)s ID: %(object_pk)s %(object_name)s %(action_type)s by %(user)s (%(message)s)'
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'personaldata-log-file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': get_env('DRIGAN_PD_LOG_FILE', 'pd.log'),
            'formatter': 'drigan'
        },
        'log-file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': get_env('DRIGAN_LOG_FILE', 'info.log'),
            'formatter': 'drigan'
        },
    },
    'loggers': {
        'root': {
            'handlers': ['mail_admins', 'log-file'],
            'level': 'INFO',
            'propagate': True,
        },
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'drigan_access_and_modification': {
            'handlers': ['personaldata-log-file'],
            'level': 'INFO',
        }
    }
}


# django-registration
ACCOUNT_ACTIVATION_DAYS = 3
LOGIN_REDIRECT_URL = '/'

# messages, bootstrap compability
MESSAGE_TAGS = {
    messages.ERROR: 'danger',
}

# DATETIME_FORMAT =

# if True - everybody can create event
DRIGAN_CREATE_EVENTS_WITHOUT_PERMISSION = False

# if True - organizer can make participants list visible for other people
DRIGAN_CAN_CHANGE_LIST_VISIBILITY = False

TEST_RUNNER = "django.test.runner.DiscoverRunner"
