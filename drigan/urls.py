from django.conf import settings
from django.conf.urls import include, patterns, url
from django.conf.urls.static import static
from django.contrib import admin
from drigan.views import StartView
# Uncomment the next two lines to enable the admin:

admin.autodiscover()

import events.urls, dynamic_forms.urls, run_manager.urls, run_ranking.urls

urlpatterns = patterns('',
    url(r'^$', StartView.as_view(), name="index"),
    url(r'^events/', include('events.urls',)),

    url(r'^forms/', include(events.urls.dynform_url_overrides + dynamic_forms.urls.urlpatterns)),

    url(r'^run/', include('run_manager.urls')),
    url(r'^ranking/', include('run_ranking.urls')),
    # Examples:
    # url(r'^$', 'drigan.views.home', name='home'),
    # url(r'^drigan/', include('drigan.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),


    url(r'^accounts/', include('drigan_registration.urls')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
