Podstawowe informacje
=====================

Aplikacja pozwala na tworzenie serwisów rejestrującyh harcerzy (i nie tylko!)
na różne imprezy.

Aplikacja aktualnie jest w fazie rozwoju i testów.

Rejestruj swoją imprezę za pomocą naszej aplikacji
--------------------------------------------------

W ramach testów w *organiczonym zakresie* aplikacja jest dostępna dla
członków ZHP chcących przeprowadzić rejestrację na swoją imprezę
za jej pomocą.

Zasady:

* Ilość imprez na które rejestracja odbywa się w tym samym czasie jest
  ograniczona.

* Zanim zgodzimy się przeprowadzić rejestrację na naszym serwerze. musimy
  otrzymać wiarygodne potwierdzenie że:

  * Kontaktuje się z nami organizator imprezy.
  * Organizator imprezy ma zgodę odpowiednich władz nadrzędnych na organizację.

  Na przykład:

  * Rejestracja na imprezy centralne wymaga zgody od Szefowej Biura GK
    ZHP.
  * Rejestracja na imprezy chorągwiane/hufcowe wymaga potwierdzenia od
    *osoby odpowiedzialnej* na poziomoe hufca/chorągwi. Bardzo pomaga
    zgłoszenie chęci rejestracji za pomocą naszej aplikacji z oficjalnego
    mejla hufca/chorągwi w domenie ``@zhp.pl``.

* Aplikacja działa testowo. Nie gwarantujemy niczego.
* W zależności od możliwości naszych ludzi musisz się zgłosić do nas (i dopełnić
  formalności) **najpóźniej** 3-10 dni przed startem rejestracji
  na imprezę (pamiętaj: im wcześniej tym lepiej).

* **Organizator odpowiada za przetestowanie poprawności działania formularza**
  Jeśli Twoi uczestnicy będą mieli problem z rejestracją to jest bardziej Twój
  problem... my oczywiście postaramy się go naprawić o rozwiązać, ale lepiej
  to zrobić zanim rejestracja ruszy, nie?

Funkcjonalności systemu
-----------------------

* Ludzie mogą rejestrować się na Twoje wydarzenie.
* Masz ciągły podgląd rejestracji.
* W swoim formularzu rejestracji możesz wpisać takie pola jakie są Ci potrzebne.


Ograniczenia systemu
--------------------

Główne ograniczenia to:

* Przyjuje zgłoszenia dla pojedyńczych osób,
  tj. drużynowy nie może wpisać swojej drużyny (ale może załączyć np.
  plik z danymi drużyny)
* Nie rejestrujemy wpłat (można załączyć potwierdzenie wpłaty)
* Nie mamy możliwości potwierdzenia adresu e-mail (ale można go wpisać)
* Po zarejestrowaniu nie ma możliwości edycji zgłoszenia.
* Gwarantujemy, że dane Twojej rejestacji będą dostępne przez 14 dni po
  zakończeniu zbierania zgłoszeń. Po tym czasie dane **mogą** zostać usunięte.

  .. note::

    Docelowo dane będziemy przechowywać bezterminowo (a w każdym razie: dłużej).


Plany rozwoju
-------------

* Rejestracja drużyn
* Integracja z ESHD
* Rejestracja wpłat

Linki
-----

* Lista zmian: :doc:`./changelog`
* Znane problemy: :doc:`./known-issues`