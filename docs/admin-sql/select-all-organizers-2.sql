SELECT DISTINCT u.first_name, u.last_name, u.username, u.email, e.name FROM auth_user AS u
  INNER JOIN guardian_userobjectpermission perm ON (perm.user_id = u.id AND perm.content_type_id = (
         SELECT ct.id FROM django_content_type ct WHERE
           model = 'attraction' AND app_label = 'events'
       ))
  INNER JOIN events_event AS e ON e.id = perm.object_pk::INTEGER
  INNER JOIN events_attraction a ON a.event_id = e.id
  INNER JOIN dynamic_forms_dynamicform df ON (
       df.object_id = event_id AND
       df.content_type_id = (
         SELECT ct.id FROM django_content_type ct WHERE
           model = 'attraction' AND app_label = 'events'
       )
   )
ORDER BY u.email